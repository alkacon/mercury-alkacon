<%@page buffer="none" session="false" taglibs="c,cms,fmt" 
import="org.opencms.jsp.*,org.opencms.main.*" %>
<%-- Only add the Siteimprove code if we are in edit mode --%>
<c:if test="${cms.isEditMode}">
	<c:choose>
		<c:when test="${not empty cms.detailContent}" >
			<%-- get url of the detail content --%>
			<c:set var="pagePath" value ="${cms.detailContentSitePath}" />
		</c:when>
		<c:otherwise>	
			<%--  get  url of the current page folder --%>
			<c:set var="pagePath" value ="${cms.requestContext.folderUri}" />		
		</c:otherwise>
	</c:choose>
	<%
		// initialise Cms Action Element
		CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
		String onlineUrl = OpenCms.getLinkManager().getOnlineLink(cms.getCmsObject(),(String)pageContext.getAttribute("pagePath"));
		pageContext.setAttribute("onlineUrl",onlineUrl);
	%>
	<script async src="https://cdn.siteimprove.net/cms/overlay.js"></script>
	<script>
	var _si = window._si || [];
	 _si.push(['input', '${onlineUrl}', '32143e58aad143118a59fb45bde23e54', function()
	 { console.log('Inputted page specific url ${onlineUrl} to Siteimprove'); }]) 
	</script>
</c:if>