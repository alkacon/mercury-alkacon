<%@page
    pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>


<c:set var="settings" value="${cms.element.settings}" />
<c:set var="subType" value="${cms.element.settings.containerSubType}" />

<c:set var="DEBUG" value="${true}" />
<%-- Using error channel only because by default no output is logged in the standard 'info' channel in the log app --%>
<m:log message="container-defaults plugin: subType is '${subType}'" channel="error" test="${DEBUG}" />

<c:choose>
    <c:when test="${subType eq 'teaser-direct-col'}">
        <c:set var="subTypeSettings" value="${{
            'cssWrapper': 'box box-high',
            'pieceLayout': '1',
            'pieceSizeMobile': '8',
            'pieceSizeDesktop': '99',
            'imageRatio': '4-3',
            'imageRatioLg': 'desk',
            'hsize': '3',
            'titleOption': 'none',
            'buttonText': 'none',
            'dateFormat': 'none',
            'effect': 'none',
            'showImageCopyright': 'false'
        }}" scope="request" />
    </c:when>
    <c:otherwise>
        <c:set var="subTypeSettings" value="${null}" scope="request" />
    </c:otherwise>
</c:choose>

