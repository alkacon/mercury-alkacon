function setScrollbarSize() {
	const scrollbarWidth = window.innerWidth - document.body.clientWidth;
	document.documentElement.style.setProperty("--sbw", `${scrollbarWidth}px`);
}

setScrollbarSize();