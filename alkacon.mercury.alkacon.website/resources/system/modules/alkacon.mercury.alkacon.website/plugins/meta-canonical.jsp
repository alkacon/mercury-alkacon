<%@page
    pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="mx" tagdir="/WEB-INF/tags/xmercury" %>

<c:set var="webPathOrigin" value="/sites/web/opencms" />
<c:set var="webPathTarget" value="/sites/web/alkacon" />

<c:if test="${cms.requestContext.siteRoot eq webPathOrigin}">

    <c:if test="${not empty canonicalURL and cms.detailRequest}">
        <c:set var="dc" value="${cms.detailContent}" />

        <c:if test="${(dc.typeName eq 'm-article') and dc.categories.containsAny(['news/'])}">
            <c:set var="hreflangURLs" value="" scope="request" />
            <c:forEach var="loc" items="${['de', 'en']}">
                <mx:site-detail-page var="detailPage" sitePath="${webPathTarget}" subSitePath="${loc}" contentRootPath="${dc.rootPath}" />
                <c:set var="detailLink"><cms:link detailPage="${detailPage}">${dc.sitePath}</cms:link></c:set>
                <c:if test="${cms.locale eq loc}">
                    <c:set var="canonicalURL" value="${detailLink}${canonicalParams}" scope="request" />
                </c:if>
                <c:set var="hreflangURLs" scope="request">
                    ${hreflangURLs}<link rel="alternate" hreflang="${loc}" href="${detailLink}"><m:nl />
                </c:set>
            </c:forEach>
        </c:if>
    </c:if>

</c:if>