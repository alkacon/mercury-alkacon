<%@page
    pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>


<c:set var="isOpenCmsWeb" value="${cms.requestContext.siteRoot eq '/sites/web/opencms'}" />

<c:choose>
    <c:when test="${cms.isDetailPage and (cms.element.formatterKey eq 'm/detail/article')}">
        <c:set var="ignore" value="${cms.modifySettings({
            'keyPiecePrefacePos': 'bt',
            'keyPieceInfoPos': 'bh'
        })}" />
    </c:when>
    <c:when test="${isOpenCmsWeb}">
        <c:choose>
            <c:when test="${(fn:contains(setcms.element.settings['displayType'], 'teaser-text-tile'))}">
                <c:set var="ignore" value="${cms.modifySettings({
                    'headingInBody': 'true',
                    'dateOnTop': 'true'
                })}" />
            </c:when>
            <c:when test="${not cms.isDetailPage and (cms.element.formatterKey eq 'm/detail/faq')}">
                <c:set var="ignore" value="${cms.modifySettings({
                    'cssWrapper3': 'pivot'
                })}" />
            </c:when>
        </c:choose>
    </c:when>
</c:choose>

