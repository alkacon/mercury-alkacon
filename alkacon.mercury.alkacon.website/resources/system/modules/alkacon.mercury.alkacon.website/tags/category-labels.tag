<%@ tag pageEncoding="UTF-8"
    display-name="category-labels"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays labels for categories." %>


<%@ attribute name="categories" type="java.util.ArrayList" required="true"
    description="The categories to display." %>

<%@ attribute name="label" type="java.lang.String" required="false"
    description="Optional label that will be shown before the categories." %>

<%@ attribute name="cssWrapper" type="java.lang.String" required="false"
    description="Optional css wrapper class that will be appended to the generated categories." %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>

<c:if test="${not empty categories}">
    <div class="cat-list${not empty cssWrapper ? ' '.concat(cssWrapper) : ''}"><%----%>
        <c:if test="${not empty label}">
            <div class="cat-label">${label}</div><%----%>
        </c:if>
        <c:forEach var="category" items="${categories}" varStatus="status">
            <div class="cat-item">${category.title}</div><%----%>
        </c:forEach>
    </div><%----%>
</c:if>
