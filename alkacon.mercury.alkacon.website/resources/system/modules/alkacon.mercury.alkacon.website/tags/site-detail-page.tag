<%@ tag pageEncoding="UTF-8"
    display-name="site-detail-page"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    import="org.opencms.main.*, org.opencms.file.*, org.opencms.jsp.util.*, java.util.*"
    description="Calculates the detail page URL for a resource in a specific site." %>


<%@ attribute name="sitePath" type="java.lang.String" required="true"
    description="The root path of the site to find the detail page in." %>

<%@ attribute name="subSitePath" type="java.lang.String" required="true"
    description="The sub site of the site to find the detail page in." %>

<%@ attribute name="contentRootPath" type="java.lang.String" required="true"
    description="The root path of the content to find the detail page for." %>

<%@ attribute name="var" rtexprvalue="false" required="true"
    description="The name of the variable to store the place name in." %>

<%@ variable alias="result" name-from-attribute="var" scope="AT_END" declare="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>


<%
    String sitePath = (String)getJspContext().getAttribute("sitePath");
    String subSitePath = (String)getJspContext().getAttribute("subSitePath");
    String contentRootPath = (String)getJspContext().getAttribute("contentRootPath");
    CmsObject cms = ((CmsJspStandardContextBean)getJspContext().findAttribute("cms")).getVfs().getCmsObject();
    CmsObject cmsCopy = OpenCms.initCmsObject(cms);
    cmsCopy.getRequestContext().setSiteRoot(sitePath);
    String result = OpenCms.getADEManager().getDetailPageHandler().getDetailPage(
        cmsCopy,
        contentRootPath,
        subSitePath,
        null
    );
    getJspContext().setAttribute("result", result);
%>