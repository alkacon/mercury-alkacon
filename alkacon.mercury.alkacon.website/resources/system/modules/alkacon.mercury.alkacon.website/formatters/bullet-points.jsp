<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>


<cms:secureparams />
<mercury:init-messages>

<cms:formatter var="content" val="value">

<c:set var="setting"                value="${cms.element.setting}" />
<c:set var="cssWrapper"             value="${setting.cssWrapper}" />
<c:set var="pieceLayout"            value="${setting.pieceLayout.toInteger}" />
<c:set var="setSizeDesktop"         value="${setting.pieceSizeDesktop.toInteger}" />
<c:set var="setSizeMobile"          value="${setting.pieceSizeMobile.toInteger}" />
<c:set var="hsize"                  value="${setting.hsize.toInteger}" />
<c:set var="imageRatio"             value="${setting.imageRatio}" />
<c:set var="linkOption"             value="${setting.linkOption}" />
<c:set var="showImageCopyright"     value="${setting.showImageCopyright.toBoolean}" />
<c:set var="showImageSubtitle"      value="${setting.showImageSubtitle.toBoolean}" />
<c:set var="showImageZoom"          value="${setting.showImageZoom.toBoolean}" />

<c:set var="intro"                  value="${value.Intro}" />
<c:set var="title"                  value="${value.Title}" />
<c:set var="preface"                value="${value.Preface}" />

<c:set var="ade"                    value="${true}" />

<mercury:nl />
<div class="element type-article as-bullets ${cssWrapper}"><%----%>
<mercury:nl />

<mercury:piece
    cssWrapper="bullet-heading"
    pieceLayout="${0}"
    allowEmptyBodyColumn="${true}">

    <jsp:attribute name="heading">
        <mercury:intro-headline intro="${intro}" css="piece-heading" headline="${title}" level="${hsize}" ade="${ade}"/>
    </jsp:attribute>

    <jsp:attribute name="text">
        <mercury:heading text="${preface}" level="${7}" css="sub-header" ade="${ade}" />
    </jsp:attribute>

</mercury:piece>

<c:if test="${not empty content.valueList.Paragraph}">

    <ul class="bullets"><%----%>
        <c:forEach var="paragraph" items="${content.valueList.Paragraph}" varStatus="status">
            <mercury:section-piece
                pieceTag="li"
                pieceLayout="${pieceLayout}"
                sizeDesktop="${setSizeDesktop}"
                sizeMobile="${setSizeMobile}"
                heading="${paragraph.value.Caption}"
                image="${paragraph.value.Image}"
                imageRatio="${imageRatio}"
                text="${paragraph.value.Text}"
                link="${paragraph.value.Link}"
                linkOption="${linkOption}"
                showImageZoom="${showImageZoom}"
                showImageSubtitle="${showImageSubtitle}"
                showImageCopyright="${showImageCopyright}"
                hsize="${hsize + 1}"
                ade="${ade}"
                emptyWarning="${not status.first}"
            />
        </c:forEach>
    </ul><%----%>
    <mercury:nl />

</c:if>

</div><%----%>
<mercury:nl />

</cms:formatter>

</mercury:init-messages>
