<%@page
    pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>

<fmt:setLocale value="${cms.workplaceLocale}" />
<cms:bundle basename="alkacon.mercury.template.messages">

<cms:formatter var="content" val="value">

<c:set var="variant" value="${value.Variant}" />

<m:layout-group
    content="${content}"
    useAdditionalVariant="${(variant eq 'foot-v1') or (variant eq 'foot-v2') or (variant eq 'head-flex-bc')}">

    <jsp:attribute name="additionalVariant">
        <c:choose>
            <c:when test="${variant eq 'head-flex-bc'}">

                <c:set var="setting"            value="${cms.element.setting}" />
                <c:set var="cssWrapper"         value="${setting.cssWrapper.isSet ? ' '.concat(cms.element.settings.cssWrapper) : ''}" />

                <c:set var="noWrapper"          value="${{'cssWrapper': ''}}" />

                <c:set var="showBreadcrumbs"            value="${setting.showBreadcrumbs.useDefault(true).toBoolean}" />
                <c:set var="addBottomMargin"            value="${setting.addBottomMargin.useDefault(true).toBoolean}" />

                <c:set var="navFixType"         value="fixed" />
                <c:set var="showBreadcrumbs"    value="${false}" />

                <c:choose>
                    <c:when test="${navFixType eq 'css'}" >
                        <c:set var="fixHeader" value="sticky csssetting" />
                    </c:when>
                    <c:when test="${navFixType eq 'upscroll'}" >
                        <c:set var="fixHeader" value="sticky upscroll" />
                    </c:when>
                    <c:when test="${navFixType eq 'fixed'}" >
                        <c:set var="fixHeader" value="sticky always" />
                    </c:when>
                </c:choose>

                <c:set var="navToggleElement">
                    <div id="nav-toggle-group"><%----%>
                        <span id="nav-toggle-label"><%----%>
                            <button class="nav-toggle-btn" aria-expanded="false" aria-controls="nav-toggle-group"><%----%>
                                <span class="nav-toggle"><%----%>
                                    <span class="nav-burger"><fmt:message key="msg.page.navigation.toggle" /></span><%----%>
                                </span><%----%>
                            </button><%----%>
                        </span><%----%>
                    </div><%----%>
                    <m:nl />
                </c:set>

                <%--
                    Attention: The order in which the containers are created here
                    MUST match the order in which they are displayed below!
                    The Flex cache will output the containers correctly only in the order they have been created.
                    This is why the logoElement is duplicated before / after the meta link element.
                --%>

                <c:set var="logoElement">
                    <c:set var="sectionTypeName"><fmt:message key="type.m-section.name" /></c:set>
                    <m:container
                        type="image-minimal"
                        name="header-image"
                        css="h-logo"
                        title="${value.Title}"
                        settings="${{
                            'cssWrapper':       'header-image',
                            'showImageLink':    'true'
                        }}"
                    />
                </c:set>

                <c:set var="metaLinkElement">
                    <c:set var="linksequenceTypeName"><fmt:message key="type.m-linksequence.name" /></c:set>
                    <m:container
                        type="linksequence-header"
                        name="header-linksequence"
                        css="h-meta"
                        title="${value.Title}"
                        settings="${{
                            'cssWrapper':       'header-links',
                            'linksequenceType': 'ls-row',
                            'hsize' :           '0'
                        }}"
                    />
                </c:set>

                <c:set var="navBarElement">
                    <div class="h-nav"><%----%>
                        <c:set var="navTypeName"><fmt:message key="type.m-navigation.name" /></c:set>
                        <m:container
                            type="nav-main"
                            name="header-nav-main"
                            css="nav-main-container"
                            title="${value.Title}"
                            settings="${{
                                'configVariant':    'default'
                            }}"
                        />
                    </div><%----%>
                    <m:nl />
                </c:set>

                <c:set var="breadcrumbElement">
                    <c:if test="${showBreadcrumbs and ((not empty cms.elementsInContainers['breadcrumbs']) or cms.modelGroupElement or not cms.element.modelGroup)}">
                        <div class="h-bc"><%----%>
                            <c:set var="navTypeName"><fmt:message key="type.m-navigation.name" /></c:set>
                            <m:container
                                type="nav-breadcrumbs"
                                name="header-breadcrumbs"
                                css="fluid-item pivot"
                                title="${value.Title}"
                                settings="${noWrapper}"
                            />
                        </div><%----%>
                        <m:nl />
                    </c:if>
                </c:set>

                <m:nl />
                <header class="area-header fh header-notfixed fluid-raster-full fluid-row fluid-row-full nav-right meta-right title-right <%----%>
                    ${addBottomMargin ? ' has-margin' : ' no-margin'}
                    ${cssWrapper}
                    ${not empty addCssWrapper ? ' '.concat(addCssWrapper) : ''}"><%----%>
                    <m:nl />

                    ${configElement}

                    ${navToggleElement}

                    <div class="header-group ${fixHeader}"><%----%>

                        <div class="head notfixed"><%----%>
                            <div class="head-overlay"></div><%----%>
                            <m:nl />

                            <%--
                                Attention: The order in which the containers have been created above
                                MUST match the order in which they are displayed here!
                                The Flex cache will output the containers correctly only in the order they have been created.
                            --%>

                                <div class="h-group fluid-item pivot"><%----%>

                                ${logoElement}

                                <div class="h-info"><%----%>

                                    ${metaLinkElement}

                                    ${navBarElement}

                                </div><%----%>

                            </div><%----%>

                        </div><%----%>
                    </div><%----%>
                    <m:nl />

                    ${breadcrumbElement}

                </header><%----%>
                <m:nl />

            </c:when>

            <c:when test="${variant eq 'foot-v1'}">

                <m:nl />
                <footer class="area-foot${cssWrapper}"><%----%>

                    <div class="topfoot"><%----%>
                        <m:container
                            type="row"
                            name="topfoot"
                            css="fluid-container area-wide"
                            title="${value.Title}"
                        />
                    </div><%----%>
                    <div class="subfoot no-external"><%----%>
                        <m:container
                            type="row"
                            name="subfoot"
                            css="fluid-container area-wide"
                            title="${value.Title}"
                        />
                    </div><%----%>

                </footer><%----%>
                <m:nl />

            </c:when>
            <c:when test="${variant eq 'foot-v2'}">

                <m:nl />
                <footer class="area-foot${cssWrapper}"><%----%>

                    <div class="subfoot no-external"><%----%>
                        <m:container
                            type="row"
                            name="subfoot"
                            css="fluid-container area-wide"
                            title="${value.Title}"
                        />
                    </div><%----%>

                </footer><%----%>
                <m:nl />

            </c:when>
        </c:choose>
    </jsp:attribute>

</m:layout-group>

</cms:formatter>
</cms:bundle>