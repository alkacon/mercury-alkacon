<%@page
    pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>

<cms:formatter var="content" val="value">

<m:setting-defaults>

<c:set var="variant"            value="${value.Variant}" />
<c:set var="detailContainer"    value="${setting.detailContainer.toString}" />
<c:set var="reverseMobileOrder" value="${setting.containerOrder.toString eq 'reversed'}" />

<c:set var="customContainer"    value="${detailContainer eq 'maincust'}" />
<c:set var="mainType"           value="${customContainer ? 'special' : 'element'}" />
<c:set var="subType"            value="${setting.containerSubType.isSetNotNone ? setting.containerSubType.toString : null}" />
<c:set var="detailContainer"    value="${customContainer ? 'maincol' : detailContainer}" />

<c:set var="bgColor"            value="${setting.bgColor.isSetNotNone ? setting.bgColor.toString : null}" />

<c:set var="cssWrapper"         value="${setCssWrapper12}${not empty bgColor ? ' '.concat(bgColor) : ''}" />
<c:set var="cssRowAddition"     value="${'fluid-row '}" />

<m:container-box label="${value.Title}" boxType="model-start" />
<m:nl />

<c:choose>

    <c:when test="${variant eq '12'}">
        <%-- lr_00001 --%>
        <m:layout-row
            rowVariant="${1}"
            title="${value.Title}"
            detailContainer="${detailContainer}"
            rowCss="${cssRowAddition}fluid-row-full${cssWrapper}"
            colCss="col-12 flex-col"
            mainType="${mainType}"
            subType="${subType}"
        />
    </c:when>

    <c:when test="${(variant eq '3-9') or (variant eq '9-3') or (variant eq '4-8') or (variant eq '8-4')}">
        <%-- lr_00002 (3-9) - lr_00003 (4-8) - lr_00007 (9-3) - lr_00008 (8-4) --%>
        <c:set var="showSideLast" value="${fn:startsWith(variant, '8') or fn:startsWith(variant, '9')}" />
        <m:layout-row
            rowVariant="${2}"
            title="${value.Title}"
            sideColSize="${fn:contains(variant, '3') ? 3 : 4}"
            showSideLast="${showSideLast}"
            detailContainer="${detailContainer}"
            rowCss="${cssRowAddition}fluid-row-cols${showSideLast ? '' : ' fluid-cols-reversed'}${cssWrapper}"
            colCss="flex-col"
            mainType="${mainType}"
            subType="${subType}"
            reverseMobileOrder="${reverseMobileOrder}"
        />
    </c:when>

    <c:when test="${variant eq '6-6-md'}">
        <%-- lr_00006 --%>
        <c:set var="breakpoint" value="${setting.breakpoint.validate(['xs','lg'],'lg').toString}" />
        <fmt:setLocale value="${cms.workplaceLocale}" />
        <cms:bundle basename="alkacon.mercury.bistuemer.ebk.portal.messages"><fmt:message var="title" key="msg.ebkportal.row.6-6" /></cms:bundle>
        <m:layout-row
            rowVariant="${2}"
            title="${title}"
            sideColSize="${6}"
            showSideLast="${true}"
            breakpoint="${breakpoint}"
            detailContainer="${detailContainer}"
            rowCss="${cssRowAddition}fluid-row-cols fluid-cols-from-${breakpoint}${' '}${bgColor}${cssWrapper}"
            colCss="flex-col"
            mainType="${mainType}"
            subType="${subType}"
            reverseMobileOrder="${reverseMobileOrder}"
        />
    </c:when>

    <c:when test="${variant eq '4-4-4'}">
        <%-- lr_00009 --%>
        <fmt:setLocale value="${cms.workplaceLocale}" />
        <cms:bundle basename="alkacon.mercury.bistuemer.ebk.portal.messages"><fmt:message var="title" key="msg.ebkportal.row.4-4-4" /></cms:bundle>
        <m:layout-row
            rowVariant="${3}"
            title="${title}"
            detailContainer="${detailContainer}"
            rowCss="${cssRowAddition}fluid-row-cols${cssWrapper}"
            colCss="flex-col"
            mainType="${mainType}"
            subType="${subType}"
            reverseMobileOrder="${reverseMobileOrder}"
        />
    </c:when>

    <c:when test="${variant eq '3-3-3-3'}">
        <%-- lr_00010 --%>
        <fmt:setLocale value="${cms.workplaceLocale}" />
        <cms:bundle basename="alkacon.mercury.bistuemer.ebk.portal.messages"><fmt:message var="title" key="msg.ebkportal.row.3-3-3-3" /></cms:bundle>
        <m:layout-row
            rowVariant="${4}"
            title="${title}"
            detailContainer="${detailContainer}"
            rowCss="${cssRowAddition}fluid-row-cols${cssWrapper}"
            colCss="flex-col"
            sideColSize="${12}"
            mainType="${mainType}"
            subType="${subType}"
            reverseMobileOrder="${reverseMobileOrder}"
        />
    </c:when>

    <c:when test="${variant eq '2-2-2-2-2-2'}">
        <%-- lr_00011 --%>
        <fmt:setLocale value="${cms.workplaceLocale}" />
        <cms:bundle basename="alkacon.mercury.bistuemer.ebk.portal.messages"><fmt:message var="title" key="msg.ebkportal.row.2-2-2-2-2-2" /></cms:bundle>
        <m:layout-row
            rowVariant="${6}"
            title="${title}"
            sideColSize="${setting.xsCols.toInteger}"
            detailContainer="${detailContainer}"
            rowCss="${cssRowAddition}fluid-row-cols${cssWrapper}"
            colCss="flex-col"
            mainType="${mainType}"
            subType="${subType}"
            reverseMobileOrder="${reverseMobileOrder}"
        />
    </c:when>

    <c:when test="${variant eq 'flex-cols'}">
        <%-- Row with configurable columns - Should be used only for 'Row with 5 columns.' --%>
        <%-- Reason: Migrating from EBK Portal to another Mercury version is much easier if default rows are used. --%>
        <c:set var="colCount" value="${setting.colCount.validate(['2','3','4','5','6'],'3').toInteger}" />
        <c:set var="breakpoint" value="lg" />
        <fmt:setLocale value="${cms.workplaceLocale}" />
        <cms:bundle basename="alkacon.mercury.bistuemer.ebk.portal.messages">
            <fmt:message var="title" key="msg.ebkportal.row.flex-cols"><fmt:param>${colCount}</fmt:param></fmt:message>
        </cms:bundle>
        <m:layout-row
            rowVariant="${5}"
            title="${title}"
            colCount="${colCount}"
            breakpoint="${breakpoint}"
            detailContainer="${detailContainer}"
            rowCss="${cssRowAddition}fluid-row-cols${cssWrapper}"
            colCss="flex-col"
            mainType="${mainType}"
            subType="${subType}"
            reverseMobileOrder="${reverseMobileOrder}"
        />
    </c:when>

    <c:when test="${variant eq '4-2-2-news'}">
        <%-- 4-2-2 row special for news display on homepage --%>
        <jsp:useBean id="valueMap"              class="java.util.HashMap" />
        <c:set var="breakpoint"                 value="-lg-" />
        <c:set var="rowCss"                     value="${cssRowAddition}fluid-row-cols${cssWrapper}" />
        <c:set var="useAffix"                   value="${true}" />

        <div class="row ${useAffix ? ' affix-row ' : ''}${rowCss}"><%----%>
            <div class="col${breakpoint}6 news-col news-col-left"><%----%>
                <c:set target="${valueMap}" property="Type"         value="text-tile"/>
                <c:set target="${valueMap}" property="Name"         value="maincol"/>
                <c:set target="${valueMap}" property="Css"          value="${useAffix ? 'affix-element' : ''}" />
                <m:container value="${valueMap}" title="${title}" detailView="${false}" />
            </div>
            <div class="col${breakpoint}6 news-col news-col-right"><%----%>
                <c:set target="${valueMap}" property="Type"         value="text-tile"/>
                <c:set target="${valueMap}" property="Name"         value="sidecol"/>
                <c:set target="${valueMap}" property="Css"          value="row fluid-row fluid-row-cols row-cols-lg-2" />
                <m:container value="${valueMap}" title="${title}" detailView="${false}" />
            </div><%----%>
        </div><%----%>
        <m:nl />
    </c:when>

    <c:when test="${variant eq 'nested-detail'}">
        <m:layout-row
            rowVariant="${1}"
            title="${value.Title}"
            detailContainer="${detailContainer ne 'none' ? 'maincol' : 'none'}"
            rowCss="${cssRowAddition}fluid-row-full${cssWrapper}"
            colCss="col-12 flex-col"
            mainType="${(detailContainer eq 'element' or (not empty cms.detailContentId)) ? 'element' : 'row'}"
        />
    </c:when>

    <c:when test="${(variant eq 'area-one-row') or (variant eq 'area-side-main')}">
        <%-- la_00001 --%>
        <m:layout-row
            rowVariant="${30}"
            title="${value.Title}"
            areaCss="${variant}${cssWrapper}${cms.isEditMode ? ' oc-point-T0_L-25' : ''}"
            rowCss="fluid-container"
            setting="${setting}"
        />
    </c:when>

    <c:when test="${variant eq 'area-grid-test'}">
        <%-- special ebkportal grid test variant --%>
        <cms:include file="/system/modules/alkacon.mercury.bistuemer.ebk.portal/functions/grid-test.jsp" />
    </c:when>

    <c:otherwise>
        <fmt:setLocale value="${cms.workplaceLocale}" />
        <cms:bundle basename="alkacon.mercury.template.messages">
            <m:alert type="error">
                <jsp:attribute name="head">
                    <fmt:message key="msg.error.layout.selection">
                        <fmt:param>${variant}</fmt:param>
                        <fmt:param>${value.Title}</fmt:param>
                    </fmt:message>
                </jsp:attribute>
                <jsp:attribute name="text">
                    <c:out value="${content.filename}" />
                </jsp:attribute>
            </m:alert>
        </cms:bundle>
    </c:otherwise>
</c:choose>

<m:nl />
<m:container-box label="${value.Title}" boxType="model-end" />

</m:setting-defaults>

</cms:formatter>
