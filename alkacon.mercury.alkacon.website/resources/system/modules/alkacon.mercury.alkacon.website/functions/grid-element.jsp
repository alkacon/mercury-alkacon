<%@page
    pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>

<c:set var="setting"            value="${cms.element.setting}" />
<c:set var="cssElWrapper"       value="${setting.cssWrapper.toString}" />
<c:set var="gridType"           value="${setting.gridType.toString}" />
<c:set var="colSize"            value="${setting.colSize.toInteger}" />
<c:set var="rasterSize"         value="${setting.rasterSize.isSetNotNone ? setting.rasterSize.toString : ''}" />
<c:set var="newCss"             value="${setting.newCss.toBoolean}" />

<c:set var="cssWrapper"         value="type-grid-element " />
<c:set var="mg"                 value="my-fluid" />

<c:set var="boxColors"          value="${['theme','akzent1','akzent2','akzent3','high','body']}" />
<c:set var="boxColorCount"      value="${boxColors.size()}" />

<c:set var="gridStyles">
<style>

.fluid-marker-row {
    position: relative;
    display: block;
    font-size: 12px;
    line-height: 8px;
}

.fluid-marker {
    display: inline-block;
    height: 30px;
    line-height: 30px;
    white-space: nowrap;
    overflow: hidden;
    text-align: center;
}

.fluid-marker-sm {
    height: 15px;
    line-height: 15px;
}

.fluid-gutter {
    background-color: #0000ff33;
    width: var(--${mg}-gutter);
    max-width: var(--${mg}-gutter);
}

.fluid-side-space {
    background-color: #0000ff99;
    width: var(--${mg}-side-space);
    max-width: var(--${mg}-side-space);
}

.fluid-over {
    background-color: #0000ff77;
    width: var(--${mg}-over);
    max-width: var(--${mg}-over);
}

.fluid-raster-marker {
    background-color: #00ff0077;
    width: var(--${mg}-raster-size);
    max-width: var(--${mg}-raster-size);
}

.fluid-parent-offset {
    background-color: #ffee0066;
    width: var(--${mg}-parent-offset);
    max-width: var(--${mg}-parent-offset);
}

.fluid-parent-over {
    background-color: #ffee00aa;
    width: var(--${mg}-parent-over);
    max-width: var(--${mg}-parent-over);
}

.fluid-parent-side-space {
    background-color: #ffee00ff;
    width: var(--${mg}-parent-side-space);
    max-width: var(--${mg}-parent-side-space);
}
</style>
</c:set>


<c:if test="${not newCss and (colSize lt 12)}">
<c:set var="cssWrapper"         value="${cssWrapper}old-css " />
<c:set var="colFactor"          value="${colSize / 12}" />
<c:set var="colInner"           value="${12 / colSize - 2}" />
<c:set var="colStyles">
<style>
@media (min-width: 1014.2px) {
.old-css .fluid-row-cols {
    padding-left: var(--${mg}-over);
    padding-right: var(--${mg}-over);
}
<c:if test="${colSize lt 6}">
.old-css .fluid-row-cols > div.col-lg-${colSize}.flex-col {
    width: ${100 * colFactor}% !important;
}
</c:if>
.old-css .fluid-row-cols > div.flex-col:first-of-type > .fluid-item {
    padding-left: var(--${mg}-box-padding-base);
}
.old-css .fluid-row-cols > div.flex-col:last-of-type > .fluid-item {
    padding-right: var(--${mg}-box-padding-base);
}
}
</style>
</c:set>
</c:if>

<c:choose>

<c:when test="${gridType eq 'ruler-detail'}">
<div class="${cssWrapper}gt-${gridType}">
${gridStyles}

<div class="fluid-marker-row fluid-raster-full">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-large</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-over">over</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-full">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-full</div><%----%>
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
</div>

<c:forTokens var="size" items="large,small" delims = ",">
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-over">over</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-parent-offset">parent-offset</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-parent-over">parent-over</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-parent-offset">parent-offset</div><%----%>
    <div class="fluid-marker fluid-parent-side-space">parent-side-space</div><%----%>
</div>
</c:forTokens>
</div>

</c:when>




<c:when test="${gridType eq 'ruler'}">

<div class="${cssWrapper}gt-${gridType}">
${gridStyles}

<div class="fluid-marker-row fluid-raster-full"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-full</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>
<div class="fluid-marker-row fluid-raster-large"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-large</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>
<div class="fluid-marker-row fluid-raster-small"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-small</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>

</div>

</c:when>




<c:when test="${gridType eq 'col-grid'}">

<c:set var="cssWrapper"         value="${cssWrapper}gt-${gridType} gt-${gridType}-${colSize}" />
<c:set var="colCount"           value="${12 / colSize}" />

<main class="${cssWrapper} area-content area-one-row" id="main-content">
${colStyles}
<div class="fluid-raster-large">

<c:choose>

    <c:when test="${colCount gt 1}">
        <div class="row fluid-row fluid-row-cols ${rasterSize}"><%----%>
        <c:forEach begin="0" end="${colCount - 1}" varStatus="loop"><%----%>
            <c:set var="boxColor" value="${boxColors.get(loop.index % boxColorCount)}" />
            <c:set var="cssElxWrapper" value="${fn:replace(cssElWrapper, '(type)', boxColor)}" />

            <div class="col-lg-${colSize} flex-col"><%----%>
                <div class="element pivot fluid-item ${cssElxWrapper}"><%----%>
                    <em>content</em><%----%>
                </div><%-- /element pivot --%>
            </div><%-- /col --%>
        </c:forEach>
        </div><%-- /row --%>
        <m:nl />
    </c:when>

    <c:otherwise>
        <div class="row fluid-row fluid-row-full ${rasterSize}">
            <c:set var="cssElxWrapper" value="${fn:replace(cssElWrapper, '(type)', 'theme')}" />
            <div class="col-${colSize} flex-col">
                <div class="element pivot fluid-item ${cssElxWrapper}">
                    <em>content</em>
                </div><%-- /element pivot --%>
            </div><%-- /col --%>
        </div><%-- /row --%>
        <m:nl />
    </c:otherwise>

</c:choose>

</div>
</main>

</c:when>

<c:otherwise>
<div>
<h1>Unknown grid type selected</h1>
</div>
</c:otherwise>

</c:choose>



