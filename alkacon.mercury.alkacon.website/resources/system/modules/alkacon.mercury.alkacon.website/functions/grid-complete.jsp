<%@page
    pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags/mercury" %>


<div>


<c:set var="mg" value="my-fluid" />
<style>

.fluid-marker-row {
    position: relative;
    display: block;
    font-size: 12px;
    line-height: 8px;
}

.fluid-marker {
    display: inline-block;
    height: 30px;
    line-height: 30px;
    white-space: nowrap;
    overflow: hidden;
    text-align: center;
}

.fluid-marker-sm {
    height: 15px;
    line-height: 15px;
}

.fluid-gutter {
    background-color: #0000ff33;
    width: var(--${mg}-gutter);
    max-width: var(--${mg}-gutter);
}

.fluid-side-space {
    background-color: #0000ff99;
    width: var(--${mg}-side-space);
    max-width: var(--${mg}-side-space);
}

.fluid-over {
    background-color: #0000ff77;
    width: var(--${mg}-over);
    max-width: var(--${mg}-over);
}

.fluid-raster-marker {
    background-color: #00ff0077;
    width: var(--${mg}-raster-size);
    max-width: var(--${mg}-raster-size);
}

.fluid-parent-offset {
    background-color: #ffee0066;
    width: var(--${mg}-parent-offset);
    max-width: var(--${mg}-parent-offset);
}

.fluid-parent-over {
    background-color: #ffee00aa;
    width: var(--${mg}-parent-over);
    max-width: var(--${mg}-parent-over);
}

.fluid-parent-side-space {
    background-color: #ffee00ff;
    width: var(--${mg}-parent-side-space);
    max-width: var(--${mg}-parent-side-space);
}
</style>

<div class="fluid-marker-row fluid-raster-full">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-large</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-over">over</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-full">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-full</div><%----%>
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
</div>

<c:forTokens var="size" items="large,small" delims = ",">
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-over">over</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-over">over</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-parent-offset">parent-offset</div><%----%>
    <div class="fluid-marker fluid-gutter">gutter</div><%----%>
    <div class="fluid-marker fluid-parent-over">parent-over</div><%----%>
</div>
<div class="fluid-marker-row fluid-raster-${size}">
    <div class="fluid-marker fluid-side-space">side-space</div><%----%>
    <div class="fluid-marker fluid-raster-marker">raster-${size}</div><%----%>
    <div class="fluid-marker fluid-parent-offset">parent-offset</div><%----%>
    <div class="fluid-marker fluid-parent-side-space">parent-side-space</div><%----%>
</div>
</c:forTokens>

<p>&nbsp;</p>

<div class="fluid-container fluid-raster-large">

<div class="row fluid-row fluid-row-full fluid-row-fill box-colors box-akzent1 fluid-raster-full">
    <div class="pivot">Lorem ipsum</div><%----%>
</div>
<div class="row fluid-row fluid-row-full fluid-row-fill box-colors box-akzent1 fluid-raster-large">
    <div class="pivot">Lorem ipsum</div><%----%>
</div>
<div class="row fluid-row fluid-row-full fluid-row-fill box-colors box-akzent1 fluid-raster-small">
    <div class="pivot">Lorem ipsum</div><%----%>
</div>

</div>


<p>&nbsp;</p>

<main class="area-content area-one-row" id="main-content">
<div class="fluid-container fluid-raster-large">

<div class="fluid-marker-row fluid-raster-full"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-full</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>
<div class="fluid-marker-row fluid-raster-large"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-large</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>
<div class="fluid-marker-row fluid-raster-small"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-small</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>

<div class="row fluid-row fluid-row-cols fluid-cols-from-xs fluid-row-fill" style="background: #fbb;">
<div class="col-6 flex-col">
<div class="element pivot fluid-item box box-akzent1">
<div style="background: #999;">
<em>box fill</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-6 flex-col">
<div class="element pivot fluid-item box box-akzent1">
<div style="background: #999;">
<em>box fill</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
</div><%-- /row --%>

<div class="row fluid-row fluid-row-cols fluid-row-fill" style="background: #fbb;">
<div class="col-lg-6 flex-col">
<div class="element pivot fluid-item">
<em>Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen.</em>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-lg-6 flex-col">
<div class="element pivot fluid-item">
<em>Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen.</em>
</div><%-- /element pivot --%>
</div><%-- /col --%>
</div><%-- /row --%>

<div class="row fluid-row fluid-row-cols fluid-cols-from-xs fluid-row-boxed" style="background: #fbb;">
<div class="col-6 flex-col">
<div class="element pivot fluid-item box box-akzent3">
<div style="background: #999;">
<em>box boxed</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-6 flex-col">
<div class="element pivot fluid-item box box-akzent3">
<div style="background: #999;">
<em>box boxed</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
</div><%-- /row --%>

<div class="row fluid-row fluid-row-cols fluid-row-boxed" style="background: #fbb;">
<div class="col-lg-6 flex-col">
<div class="element pivot fluid-item">
<em>Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen.</em>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-lg-6 flex-col">
<div class="element pivot fluid-item">
<em>Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen.</em>
</div><%-- /element pivot --%>
</div><%-- /col --%>
</div><%-- /row --%>

<p>&nbsp;</p>

<div class="fluid-marker-row fluid-raster-full"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-full</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>
<div class="fluid-marker-row fluid-raster-large"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-large</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>
<div class="fluid-marker-row fluid-raster-small"><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-raster-marker">raster-small</div><%----%>
    <div class="fluid-marker fluid-marker-sm fluid-side-space"></div><%----%>
</div><%----%>

<div class="row fluid-row fluid-row-cols fluid-cols-from-xs fluid-row-fill" style="background: #fbb;">
<div class="col-lg-4 flex-col">
<div class="element pivot fluid-item box box-akzent1">
<div style="background: #999;">
<em>box fill</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-lg-4 flex-col">
<div class="element pivot fluid-item box box-akzent1">
<div style="background: #999;">
<em>box fill</em>
</div>
</div><%-- /element pivot --%>
<div class="element pivot fluid-item box box-akzent1">
<div style="background: #999;">
<em>box fill</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-lg-4 flex-col">
<div class="element pivot fluid-item box box-akzent1">
<div style="background: #999;">
<em>box fill</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
</div><%-- /row --%>


<div class="row fluid-row fluid-row-cols fluid-cols-from-xs fluid-row-boxed" style="background: #fbb;">
<div class="col-lg-4 flex-col">
<div class="element pivot fluid-item box box-akzent3">
<div style="background: #999;">
<em>box boxed</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-lg-4 flex-col">
<div class="element pivot fluid-item box box-akzent3">
<div style="background: #999;">
<em>box boxed</em>
</div>
</div><%-- /element pivot --%>
<div class="element pivot fluid-item box box-akzent3">
<div style="background: #999;">
<em>box boxed</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-lg-4 flex-col">
<div class="element pivot fluid-item box box-akzent3">
<div style="background: #999;">
<em>box boxed</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
</div><%-- /row --%>


<p>&nbsp;</p>


<div class="row" style="background: #fbb;">
<div class="col-6 flex-col">
<div class="element pivot box box-akzent2">
<div style="background: #999;">
<em>box (not fluid)</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
<div class="col-6 flex-col">
<div class="element pivot box box-akzent2">
<div style="background: #999;">
<em>box (not fluid)</em>
</div>
</div><%-- /element pivot --%>
</div><%-- /col --%>
</div><%-- /row --%>


</div>
</main>


</div>
