/*
 * This library is part of OpenCms -
 * the Open Source Content Management System
 *
 * Copyright (c) Alkacon Software GmbH (http://www.alkacon.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about Alkacon Software, please see the
 * company website: http://www.alkacon.com
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package alkacon.mercury.alkacon.intranet;

import org.opencms.configuration.CmsParameterStore;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.i18n.CmsLocaleManager;
import org.opencms.jsp.CmsJspActionElement;
import org.opencms.lock.CmsLockUtil;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.ocee.keygenerator.CmsLicenseKeyGenerator;
import org.opencms.util.CmsMacroResolver;
import org.opencms.util.CmsStringUtil;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.content.CmsXmlContentFactory;
import org.opencms.xml.types.I_CmsXmlContentValue;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.text.StringEscapeUtils;

/**
 * Action element for V1 license key generator page.
 */
public class CmsOldKeyGenActionElement extends CmsJspActionElement {

    /** Date format for license validity field. */
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    /** Default locale to use. */
    public static final String DEFAULT_LOCALE = "de";

    /** License type constant. */
    public static final String TYPE_DEV = CmsLicenseKeyGenerator.LICENSE_TYPES[2];

    /** License type constant. */
    public static final String TYPE_EVAL = CmsLicenseKeyGenerator.LICENSE_TYPES[0];

    /** License type constant. */
    public static final String TYPE_FULL = CmsLicenseKeyGenerator.LICENSE_TYPES[3];

    /** License type constant. */
    public static final String TYPE_PROV = CmsLicenseKeyGenerator.LICENSE_TYPES[1];

    /**
     * Constant to determine the cluster type of distribution for a key.
     * <p>
     */
    private static final String DISTRIBUTION_CLUSTER = "CLUSTER";

    /**
     * Constant to determine the single type of distribution for a key.
     * <p>
     */
    private static final String DISTRIBUTION_SERVER = "SERVER";

    /**
     * Constant to determine the server type of distribution for a key.
     * <p>
     */
    private static final String DISTRIBUTION_SINGLE = "SINGLE";

    /** Logger. */
    private static final Log LOG = CmsLog.getLog(CmsOldKeyGenActionElement.class);

    /** Error message to display. */
    private String m_error;

    /** Javascript-escaped redirect location, for use by Javascript. */
    private String m_javascriptLocation;

    /**
     * Creates a new instance.
     *
     * @param context
     *            the page context
     * @param request
     *            the servlet request
     * @param response
     *            the servlet response
     */
    public CmsOldKeyGenActionElement(PageContext context, HttpServletRequest request, HttpServletResponse response) {

        super(context, request, response);
        Map<String, String> parameters = new HashMap<>();
        for (Object key : request.getParameterMap().keySet()) {
            parameters.put((String)key, request.getParameter((String)key));
        }
    }

    /**
     * Main action method called by the license key generation page to handle the
     * form post. Error-handling wrapper around actionInternal.
     *
     * @throws IOException
     *             in case of an IO error
     */
    public void action() throws IOException {

        try {
            actionInternal();
        } catch (Throwable e) {
            LOG.error(e.getLocalizedMessage(), e);
            CmsMacroResolver resolver = new CmsMacroResolver();
            resolver.setCmsObject(getCmsObject());
            resolver.setMessages(OpenCms.getWorkplaceManager().getMessages(Locale.GERMAN));
            m_error = resolver.resolveMacros(e.getLocalizedMessage());
        }
    }

    /**
     * Handles the form post from the license key generation page.
     * <p>
     * In case of an error, the m_error field is set, otherwise the m_location
     *
     * @throws Exception
     */
    public void actionInternal() throws Exception {

        HttpServletRequest request = getRequest();

        CmsObject cms = getCmsObject();
        String localeStr = CmsParameterStore.getInstance().getValue(cms, "ocee-keygen-locale");
        if (localeStr == null) {
            localeStr = DEFAULT_LOCALE;
        }
        Locale locale = CmsLocaleManager.getLocale(localeStr);

        if (cms.getRequestContext().getCurrentProject().isOnlineProject()) {
            return;
        }
        String productFilename = request.getParameter("productfilename");
        if (CmsStringUtil.isEmptyOrWhitespaceOnly(productFilename)) {
            throw new Exception("Kein Produkt ausgewaehlt");
        }
        CmsResource productRes = cms.readResource(productFilename);
        CmsFile productFile = cms.readFile(productRes); // Already read the file here so we fail early if it isn't there

        /* ------------------ Key generation --------------------- */

        String formsubmit = request.getParameter("formsubmit");
        if (formsubmit == null) {
            return;
        }

        String custName = request.getParameter("customername");
        if (custName != null) {
            custName = custName.trim();
        }
        if (!CmsIntranetConstants.CUSTOMER_NAME_PATTERN_COMPILED.matcher(custName).matches()) {
            throw new Exception(
                "Ungueltiger Kundenname, nur Buchstaben, Ziffern, Leerzeichen, Punkt, Komma und Strich erlaubt.");
        }

        String strType = request.getParameter("keytype");

        String strValidTill = request.getParameter("validtill");
        Date validTill = null;
        if ((strValidTill != null) && (Arrays.asList(TYPE_EVAL, TYPE_PROV).contains(strType))) {
            try {
                validTill = DATE_FORMAT.parse(strValidTill);
            } catch (ParseException e) {
                LOG.warn(e.getLocalizedMessage(), e);
                throw new Exception("Ungueltiges Ablaufdatum");
            }
        }

        String activationCode = request.getParameter("activationcode");
        if (activationCode != null) {
            activationCode = activationCode.trim();
            if (CmsStringUtil.isEmptyOrWhitespaceOnly(activationCode)) {
                activationCode = null;
            }
        }

        String strDevelopmentHoursDividedBy4 = request.getParameter("developmenthours");
        int developmentHoursDividedBy4 = 3;
        if (!CmsStringUtil.isEmptyOrWhitespaceOnly(strDevelopmentHoursDividedBy4)) {
            developmentHoursDividedBy4 = Integer.parseInt(strDevelopmentHoursDividedBy4.trim());
        }

        String key = null;
        long realValidTill;
        if (TYPE_DEV.equals(strType)) {
            realValidTill = developmentHoursDividedBy4;
        } else if (validTill != null) {
            realValidTill = validTill.getTime();
        } else {
            realValidTill = 0;
        }

        String productType = request.getParameter("productname");
        if (productType == null) {
            productType = "";
        }
        productType = productType.toLowerCase();
        String distro;
        if (productType.contains("1sl")) {
            distro = DISTRIBUTION_SINGLE;
        } else if (productType.contains("2cnl")) {
            distro = DISTRIBUTION_CLUSTER;
        } else {
            throw new Exception("Produkttyp nicht richtig initialisiert");
        }

        CmsLicenseKeyGenerator generator = new CmsLicenseKeyGenerator();
        key = generator.generateLicenseKey(
            distro,
            strType,
            custName,
            System.currentTimeMillis(),
            realValidTill,
            activationCode);
        if ("_INVALID_ACTIVATION_KEY_".equals(key)) {
            throw new Exception("Ungueltiger Activation Code");
        }

        String fileLicenseType = null;

        if (TYPE_DEV.equals(strType)) {
            fileLicenseType = "Development";
        } else if (TYPE_FULL.equals(strType)) {
            fileLicenseType = "Production";
        } else if (TYPE_EVAL.equals(strType)) {
            fileLicenseType = "Evaluation";
        } else if (TYPE_PROV.equals(strType)) {
            fileLicenseType = "Provisional";
        } else {
            throw new Exception("Ungueltiger Lizenztyp");
        }

        /* ------------------ Saving the key --------------------- */

        CmsXmlContent productContent = CmsXmlContentFactory.unmarshal(cms, productFile);
        int appendIndex = productContent.getValues("Key", locale).size();
        I_CmsXmlContentValue keyValue = productContent.addValue(cms, "Key", locale, appendIndex);
        String keyPath = keyValue.getPath();

        productContent.getValue(keyPath + "/Type", locale).setStringValue(cms, fileLicenseType);
        productContent.getValue(keyPath + "/Name", locale).setStringValue(cms, custName);

        String distroXml = null;
        if (DISTRIBUTION_SINGLE.equals(distro)) {
            distroXml = CmsKeyDistributionType.oceeV1ServerEnhancement.toString();
        } else {
            distroXml = CmsKeyDistributionType.oceeV1Cluster.toString();
        }
        productContent.getValue(keyPath + "/Distribution", locale).setStringValue(cms, distroXml);
        productContent.getValue(keyPath + "/Date", locale).setStringValue(cms, "" + System.currentTimeMillis());
        if (Arrays.asList(TYPE_PROV, TYPE_EVAL).contains(strType)) {
            productContent.addValue(cms, keyPath + "/ExpirationDate", locale, 0);
            productContent.getValue(keyPath + "/ExpirationDate", locale).setStringValue(cms, "" + realValidTill);
        }
        productContent.getValue(keyPath + "/LicenceKey", locale).setStringValue(cms, key);
        if (!CmsStringUtil.isEmptyOrWhitespaceOnly(activationCode)) {
            productContent.addValue(cms, keyPath + "/ActivationKey", locale, 0);
            productContent.getValue(keyPath + "/ActivationKey", locale).setStringValue(
                cms,
                activationCode != null ? activationCode : "");
        }
        if (TYPE_DEV.equals(strType)) {
            productContent.addValue(cms, keyPath + "/DevelopmentTime", locale, 0);
            productContent.getValue(keyPath + "/DevelopmentTime", locale).setStringValue(cms, "3"); // 3*4=12h
        }
        productContent.getValue(keyPath + "/Inactive", locale).setStringValue(cms, "false");
        byte[] newContentData = productContent.marshal();
        productFile.setContents(newContentData);
        try (AutoCloseable closeable = CmsLockUtil.withLockedResources(cms, productRes)) {
            cms.writeFile(productFile);
        }

        String link = OpenCms.getLinkManager().substituteLinkForUnknownTarget(cms, productFilename);
        m_javascriptLocation = StringEscapeUtils.escapeEcmaScript(link);
    }

    /**
     * Gets the error message (not escaped for HTML).
     *
     * @return the error message
     */
    public String getError() {

        return m_error;
    }

    /**
     * Gets the Javascript redirect location (already escaped for embedding in
     * Javascript).
     *
     * @return the Javascript redirect location
     */
    public String getJavascriptLocation() {

        return m_javascriptLocation;
    }

}
