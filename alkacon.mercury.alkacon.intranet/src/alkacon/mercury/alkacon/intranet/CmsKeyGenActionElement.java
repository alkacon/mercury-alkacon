/*
 * This library is part of OpenCms -
 * the Open Source Content Management System
 *
 * Copyright (c) Alkacon Software GmbH (http://www.alkacon.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about Alkacon Software, please see the
 * company website: http://www.alkacon.com
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package alkacon.mercury.alkacon.intranet;

import org.opencms.configuration.CmsParameterStore;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.i18n.CmsLocaleManager;
import org.opencms.jsp.CmsJspActionElement;
import org.opencms.lock.CmsLockUtil;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.ocee.base.CmsLicenseInfo.LicenseType;
import org.opencms.ocee.keygenerator.LicenseKeyGenerator;
import org.opencms.util.CmsMacroResolver;
import org.opencms.util.CmsStringUtil;
import org.opencms.util.CmsUUID;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.content.CmsXmlContentFactory;
import org.opencms.xml.types.I_CmsXmlContentValue;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.text.StringEscapeUtils;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Action element for license key generator page.
 */
public class CmsKeyGenActionElement extends CmsJspActionElement {

    /** Default locale to use. */
    public static final String DEFAULT_LOCALE = "de";

    /** Logger. */
    private static final Log LOG = CmsLog.getLog(CmsKeyGenActionElement.class);

    /** Error message to display. */
    private String m_error;

    /** Javascript-escaped redirect location, for use by Javascript. */
    private String m_javascriptLocation;

    /**
     * Creates a new instance.
     *
     * @param context the page context
     * @param request the servlet request
     * @param response the servlet response
     */
    public CmsKeyGenActionElement(PageContext context, HttpServletRequest request, HttpServletResponse response) {

        super(context, request, response);
        Map<String, String> parameters = new HashMap<>();
        for (Object key : request.getParameterMap().keySet()) {
            parameters.put((String)key, request.getParameter((String)key));
        }
    }

    /**
     * Main action method called by the license key generation page to handle the form post.
     * Error-handling wrapper around actionInternal.
     *
     * @throws IOException in case of an IO error
     */
    public void action() throws IOException {

        try {
            actionInternal();
        } catch (Throwable e) {
            LOG.error(e.getLocalizedMessage(), e);
            CmsMacroResolver resolver = new CmsMacroResolver();
            resolver.setCmsObject(getCmsObject());
            resolver.setMessages(OpenCms.getWorkplaceManager().getMessages(Locale.GERMAN));
            m_error = resolver.resolveMacros(e.getLocalizedMessage());
        }

    }

    /**
     * Handles the form post from the license key generation page.
     * <p>In case of an error, the m_error field is set, otherwise the m_location
     *
     * @throws Exception
     */
    public void actionInternal() throws Exception {

        HttpServletRequest request = getRequest();

        CmsObject cms = getCmsObject();
        String localeStr = CmsParameterStore.getInstance().getValue(cms, "ocee-keygen-locale");
        if (localeStr == null) {
            localeStr = DEFAULT_LOCALE;
        }
        OpenCms.getLocaleManager();
        Locale locale = CmsLocaleManager.getLocale(localeStr);

        if (cms.getRequestContext().getCurrentProject().isOnlineProject()) {
            return;
        }
        String productFilename = request.getParameter("productfilename");
        if (CmsStringUtil.isEmptyOrWhitespaceOnly(productFilename)) {
            throw new Exception("Kein Produkt ausgewaehlt");
        }
        CmsResource productRes = cms.readResource(productFilename);
        CmsFile productFile = cms.readFile(productRes); // Already read the file here so we fail early if it isn't there

        /* ------------------ Key generation ---------------------*/

        String formsubmit = request.getParameter("formsubmit");
        if (formsubmit == null) {
            return;
        }

        String custName = request.getParameter("customername");
        if (custName != null) {
            custName = custName.trim();
        }
        if (!CmsIntranetConstants.CUSTOMER_NAME_PATTERN_COMPILED.matcher(custName).matches()) {
            throw new Exception(
                "Ungueltiger Kundenname, nur Buchstaben, Ziffern, Leerzeichen, Punkt, Komma und Strich erlaubt.");
        }

        String strType = request.getParameter("keytype");
        LicenseType type = LicenseType.valueOf(strType);
        String strValidTill = request.getParameter("validtill");
        Date validTill = null;
        if ((strValidTill != null) && (type != LicenseType.development)) {
            try {
                DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZoneUTC();
                validTill = new Date(format.parseDateTime(strValidTill + " 23:59:59").getMillis());
            } catch (IllegalArgumentException e) {
                LOG.warn(e.getLocalizedMessage(), e);
                throw new Exception("Ungueltiges Ablaufdatum");
            }
        }

        String activationCode = request.getParameter("activationcode");
        if (activationCode != null) {
            activationCode = activationCode.trim();
        }
        String strClusterSize = request.getParameter("clustersize");
        int clusterSize = Integer.parseInt(strClusterSize.trim());
        if (clusterSize <= 0) {
            throw new Exception("Ungueltige Clusterknotenzahl");
        }

        String strDevelopmentHours = request.getParameter("developmenthours");
        int developmentHours = 12;
        if (strDevelopmentHours != null) {
            developmentHours = Integer.parseInt(strDevelopmentHours.trim());
        }

        LicenseKeyGenerator generator = new LicenseKeyGenerator();

        CmsUUID billingId;
        String strBillingId = request.getParameter("billingid");
        if (!CmsStringUtil.isEmptyOrWhitespaceOnly(strBillingId)) {
            billingId = new CmsUUID(strBillingId);
        } else {
            billingId = new CmsUUID();
        }
        int productSupport = 3; // cluster
        String key = null;
        long realValidTill;
        if (type == LicenseType.development) {
            realValidTill = developmentHours;
        } else {
            realValidTill = validTill.getTime();
        }
        if (type == LicenseType.production) {
            key = generator.generateKey(activationCode, productSupport, clusterSize, realValidTill);
        } else {
            key = generator.generateKey(
                custName,
                billingId,
                type.toString(),
                productSupport,
                clusterSize,
                realValidTill,
                CmsUUID.getNullUUID(),
                "");
        }

        /* ------------------ Saving the key ---------------------*/

        CmsXmlContent productContent = CmsXmlContentFactory.unmarshal(cms, productFile);
        int appendIndex = productContent.getValues("Key", locale).size();
        I_CmsXmlContentValue keyValue = productContent.addValue(cms, "Key", locale, appendIndex);
        String keyPath = keyValue.getPath();

        productContent.getValue(keyPath + "/Type", locale).setStringValue(cms, StringUtils.capitalize(type.toString()));
        productContent.getValue(keyPath + "/Name", locale).setStringValue(cms, custName);
        productContent.getValue(keyPath + "/Distribution", locale).setStringValue(
            cms,
            CmsKeyDistributionType.oceeV2Subscription.toString());
        productContent.getValue(keyPath + "/Date", locale).setStringValue(cms, "" + System.currentTimeMillis());
        if (type != LicenseType.development) {
            productContent.addValue(cms, keyPath + "/ExpirationDate", locale, 0);
            productContent.getValue(keyPath + "/ExpirationDate", locale).setStringValue(cms, "" + realValidTill);
        }
        productContent.getValue(keyPath + "/LicenceKey", locale).setStringValue(cms, key);
        if (!CmsStringUtil.isEmptyOrWhitespaceOnly(activationCode)) {
            productContent.addValue(cms, keyPath + "/ActivationKey", locale, 0);
            productContent.getValue(keyPath + "/ActivationKey", locale).setStringValue(
                cms,
                activationCode != null ? activationCode : "");
        }

        if (type == LicenseType.development) {
            productContent.addValue(cms, keyPath + "/DevelopmentTime", locale, 0);
            productContent.getValue(keyPath + "/DevelopmentTime", locale).setStringValue(cms, "3"); // 3*4=12h
        }

        productContent.addValue(cms, keyPath + "/ClusterSize", locale, 0);
        productContent.getValue(keyPath + "/ClusterSize", locale).setStringValue(cms, "" + clusterSize);

        productContent.getValue(keyPath + "/Inactive", locale).setStringValue(cms, "false");
        byte[] newContentData = productContent.marshal();
        productFile.setContents(newContentData);
        try (AutoCloseable closeable = CmsLockUtil.withLockedResources(cms, productRes)) {
            cms.writeFile(productFile);
        }

        String link = OpenCms.getLinkManager().substituteLinkForUnknownTarget(cms, productFilename);

        m_javascriptLocation = StringEscapeUtils.escapeEcmaScript(link);
    }

    /**
     * Gets the error message (not escaped for HTML).
     *
     * @return the error message
     */
    public String getError() {

        return m_error;
    }

    /**
     * Gets the Javascript redirect location (already escaped for embedding in Javascript).
     *
     * @return the Javascript redirect location
     */
    public String getJavascriptLocation() {

        return m_javascriptLocation;
    }

}
