/*
 * File   : $Source$
 * Date   : $Date$
 * Version: $Revision$
 *
 * This library is part of OpenCms -
 * the Open Source Content Management System
 *
 * Copyright (C) 2002 - 2008 Alkacon Software (http://www.alkacon.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about Alkacon Software, please see the
 * company website: http://www.alkacon.com
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package alkacon.mercury.alkacon.intranet;

import org.opencms.file.CmsObject;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsResource;
import org.opencms.i18n.CmsLocaleManager;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.search.extractors.I_CmsExtractionResult;
import org.opencms.search.fields.CmsSearchFieldMappingType;
import org.opencms.search.fields.I_CmsSearchFieldMapping;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.content.CmsXmlContentFactory;
import org.opencms.xml.types.I_CmsXmlContentValue;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;

public class CmsSortDateSearchFieldMapping implements I_CmsSearchFieldMapping {

    /** The logger instance for this class. */
    private static final Log LOG = CmsLog.getLog(CmsSortDateSearchFieldMapping.class);

    private static final long serialVersionUID = 0;

    private String m_param;
    private CmsSearchFieldMappingType m_type;

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#getDefaultValue()
     */
    @Override
    public String getDefaultValue() {

        return String.valueOf(System.currentTimeMillis());
    }

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#getParam()
     */
    @Override
    public String getParam() {

        return m_param;
    }

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#getStringValue(org.opencms.file.CmsObject,
     *      org.opencms.file.CmsResource,
     *      org.opencms.search.extractors.I_CmsExtractionResult, java.util.List,
     *      java.util.List)
     */
    @Override
    public String getStringValue(
        CmsObject cms,
        CmsResource res,
        I_CmsExtractionResult extractionResult,
        List<CmsProperty> properties,
        List<CmsProperty> propertiesSearched) {

        String result = null;
        try {
            CmsXmlContent content = CmsXmlContentFactory.unmarshal(cms, cms.readFile(res));
            OpenCms.getLocaleManager();
            Locale mainLocale = CmsLocaleManager.getLocale("de");
            I_CmsXmlContentValue value = content.getValue("ExpirationDate", mainLocale);
            if (value == null) {
                value = content.getValue("ActivationDate", mainLocale);
                if (value != null) {
                    long millis = Long.parseLong(value.getStringValue(cms));
                    if (millis > 0) { // ignore obviously "wrong" or "unset" dates <= 0
                        // Add one year to the activation date.
                        Instant instant = Instant.ofEpochMilli(millis);
                        ZonedDateTime dt = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
                        dt = dt.plusYears(1);
                        result = Long.toString(dt.toInstant().toEpochMilli());
                    }
                }
            } else {
                result = value.getStringValue(cms);
            }
        } catch (Throwable t) {
            LOG.error(t, t);
        }
        return null == result ? getDefaultValue() : result;
    }

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#getType()
     */
    @Override
    public CmsSearchFieldMappingType getType() {

        return CmsSearchFieldMappingType.DYNAMIC;
    }

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#setDefaultValue(java.lang.String)
     */
    @Override
    public void setDefaultValue(String defaultValue) {

        // Should not be set
    }

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#setParam(java.lang.String)
     */
    @Override
    public void setParam(String param) {

        m_param = param;
    }

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#setType(org.opencms.search.fields.CmsSearchFieldMappingType)
     */
    @Override
    public void setType(CmsSearchFieldMappingType type) {

        m_type = type;
    }

    /**
     * @see org.opencms.search.fields.I_CmsSearchFieldMapping#setType(java.lang.String)
     */
    @Override
    public void setType(String type) {

        m_type = CmsSearchFieldMappingType.valueOf(type);
    }

}
