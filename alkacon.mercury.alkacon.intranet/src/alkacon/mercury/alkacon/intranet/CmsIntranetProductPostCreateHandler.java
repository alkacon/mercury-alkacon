
package alkacon.mercury.alkacon.intranet;

import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.collectors.I_CmsCollectorPostCreateHandler;
import org.opencms.jsp.util.CmsJspContentAccessBean;
import org.opencms.main.CmsLog;
import org.opencms.util.CmsResourceTranslator;
import org.opencms.util.CmsUUID;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.types.I_CmsXmlContentValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;

import org.joda.time.DateTime;

/**
 * A post create handler that adds a reference to the customer content in a
 * created intranet product.
 * <p>
 *
 * @see I_CmsCollectorPostCreateHandler for more information on where post
 *      create handlers can be configured.
 */
public class CmsIntranetProductPostCreateHandler implements I_CmsCollectorPostCreateHandler {

    /** The log object for this class. */
    private static final Log LOG = CmsLog.getLog(CmsIntranetProductPostCreateHandler.class);

    /**
     * @see org.opencms.file.collectors.I_CmsCollectorPostCreateHandler#onCreate(org.opencms.file.CmsObject,
     *      org.opencms.file.CmsResource, boolean)
     */
    public void onCreate(CmsObject cms, CmsResource createdResource, boolean copyMode) {

        // there are no parameters defined. Just return without modifying the created
        // resource.
        return;

    }

    /**
     * Adds a reference to the customer content to the newly created resource using
     * the <code>config</code> parameter.
     * <p>
     *
     * @param cms the current user's CMS context
     * @param createdResource the resource which has been created
     * @param copyMode <code>true</code> if the user chose one of the elements in the collector list as a model
     * @param config the customer content this product belongs to
     *
     * @see org.opencms.file.collectors.I_CmsCollectorPostCreateHandler#onCreate(org.opencms.file.CmsObject,
     *      org.opencms.file.CmsResource, boolean, java.lang.String)
     */
    public void onCreate(CmsObject cms, CmsResource createdResource, boolean copyMode, String params) {

        if ((null != params) && !copyMode) {

            try {
                List<String> param = Arrays.asList(params.split("#"));
                String customerRelation = param.get(0);

                CmsJspContentAccessBean content = new CmsJspContentAccessBean(cms, createdResource);
                CmsXmlContent rawXmlContent = (CmsXmlContent)content.getRawContent();

                setXmlValue(cms, rawXmlContent, "CustomerRelation", customerRelation);

                if (content.getTypeName().equals("supportOcee")) {
                    // OCEE has been created
                    CmsResource customerResource = cms.readResource(customerRelation);
                    CmsJspContentAccessBean customerContent = new CmsJspContentAccessBean(cms, customerResource);
                    // set installation / key name
                    String customerName = customerContent.getValue().get("Name").getToString();
                    setXmlValue(cms, rawXmlContent, "KeyName", createOceeKeyName(cms, customerName));
                    // generate random billing id
                    CmsUUID billingId = new CmsUUID();
                    setXmlValue(cms, rawXmlContent, "BillingId", billingId.toString());
                } else if (content.getTypeName().equals("supportIncident")) {
                    // Incident support has been created
                    CmsResource customerResource = cms.readResource(customerRelation);
                    CmsJspContentAccessBean customerContent = new CmsJspContentAccessBean(cms, customerResource);
                    String customerName = customerContent.getValue().get("Name").getToString();
                    setXmlValue(cms, rawXmlContent, "AccountID", createIncidentAccountID(cms, customerName));
                }

                CmsFile file = content.getFile();
                file.setContents(rawXmlContent.marshal());

                cms.lockResource(file);
                cms.writeFile(file);
                cms.unlockResource(file);
            } catch (Exception e) {
                LOG.error("Failed to connect product " + createdResource.getRootPath() + " to customer " + params, e);
            }
        }
    }

    private String createIncidentAccountID(CmsObject cms, String input) {

        String translated = translateName(cms, input);
        DateTime dt = new DateTime();
        String alphanumericPart = translated.replaceAll("[^a-zA-Z0-9]", "");
        String prefix = alphanumericPart.substring(0, Math.min(6, alphanumericPart.length()));
        prefix = prefix.toUpperCase();
        String result = prefix + "-" + String.format("%02d%02d", dt.getYearOfCentury(), dt.getMonthOfYear()) + "-OC10";
        return result;
    }

    private String createOceeKeyName(CmsObject cms, String input) {

        String result = translateName(cms, input);
        // now handle all other chars that are not wanted
        result = result.replaceAll("\\s", " ");
        result = result.replaceAll("[^a-zA-Z0-9,-. ]", "");
        result = result.replaceFirst("^[^a-zA-Z0-9.]+", "");
        result = result.replaceFirst("[^a-zA-Z0-9.]+$", "");
        if (result.length() < 2) {
            result = "NAME" + result;
        }
        return result;
    }

    private void setXmlValue(CmsObject cms, CmsXmlContent rawXmlContent, String xpath, String value) {

        I_CmsXmlContentValue xmlValue = rawXmlContent.getValue(xpath, Locale.GERMAN, 0);
        if (xmlValue == null) {
            xmlValue = rawXmlContent.addValue(cms, xpath + "[1]", Locale.GERMAN, 0);
        }
        xmlValue.setStringValue(cms, value);
    }

    private String translateName(CmsObject cms, String input) {

        // use the default file translations
        List<String> fileTranslations = new ArrayList<String>();
        fileTranslations.addAll(Arrays.asList(cms.getRequestContext().getFileTranslator().getTranslations()));
        // remove the rules that handle empty chars etc.
        fileTranslations.remove("s#[\\s]+#-#g");
        fileTranslations.remove("s#\\\\#/#g");
        fileTranslations.remove("s#[^0-9a-zA-Z_$~\\.\\-\\/]#!#g");
        fileTranslations.remove("s#!+##g");
        fileTranslations.remove("s#-+#-#g");
        fileTranslations.remove("s#^(.{250}).*$#$1#");
        String[] reducedRules = fileTranslations.toArray(new String[0]);
        CmsResourceTranslator keyTranslator = new CmsResourceTranslator(reducedRules, true);
        // translate using reduced rules
        return keyTranslator.translateResource(input);
    }
}
