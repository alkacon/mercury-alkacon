/*
 * This library is part of OpenCms -
 * the Open Source Content Management System
 *
 * Copyright (c) Alkacon Software GmbH (http://www.alkacon.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about Alkacon Software, please see the
 * company website: http://www.alkacon.com
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package alkacon.mercury.alkacon.intranet;

import java.util.regex.Pattern;

public class CmsIntranetConstants {

    /** Pattern to match the customernames with.<p> */
    public static final String CUSTOMER_NAME_PATTERN = "^[a-zA-Z0-9.][a-zA-Z0-9,-. ]*[a-zA-Z0-9.]$";

    /** Compiled regex for checking customer names. */
    public static final Pattern CUSTOMER_NAME_PATTERN_COMPILED = Pattern.compile(CUSTOMER_NAME_PATTERN);

    /** Module name for the intranet. */
    public static final String MODULE_NAME = "alkacon.mercury.intranet";

}
