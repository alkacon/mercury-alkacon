/*
 * This library is part of OpenCms -
 * the Open Source Content Management System
 *
 * Copyright (c) Alkacon Software GmbH (http://www.alkacon.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about Alkacon Software, please see the
 * company website: http://www.alkacon.com
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package alkacon.mercury.alkacon.intranet;

import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.CmsVfsResourceNotFoundException;
import org.opencms.lock.CmsLockUtil;
import org.opencms.main.CmsException;
import org.opencms.main.CmsIllegalArgumentException;
import org.opencms.main.CmsLog;
import org.opencms.util.CmsStringUtil;
import org.opencms.xml.CmsXmlUtils;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.content.CmsXmlContentFactory;
import org.opencms.xml.types.I_CmsXmlContentValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;

public class CmsIntranetCustomerAndProductConverter {

    public static final List<String> noRelationProducts = Arrays.asList(
        "product.name.ocee-1acnl",
        "product.name.ocee-up12");

    public static final List<String> relationProducts = Arrays.asList(
        "product.name.ocee-1sl-eval",
        "product.name.ocee-1sl-01",
        "product.name.ocee-1sl-02",
        "product.name.ocee-1sl-03",
        "product.name.ocee-2cnl-eval",
        "product.name.ocee-2cnl-01",
        "product.name.ocee-2cnl-02",
        "product.name.ocee-2cnl-03",
        "product.name.sup-premium-12",
        "product.name.sup-incident-10");
    public static boolean SAVE_ENABLED = true;

    private static final Locale LOCALE = Locale.GERMAN;
    private static final Log LOG = CmsLog.getLog(CmsIntranetCustomerAndProductConverter.class);
    private CmsObject m_cms;

    private String m_customerDir;

    public CmsIntranetCustomerAndProductConverter(CmsObject cms) {

        m_cms = cms;
    }

    public void processCustomerFile(CmsResource customer) throws Exception {

        CmsObject cms = m_cms;
        CmsFile file = cms.readFile(customer);
        CmsXmlContent content = CmsXmlContentFactory.unmarshal(cms, file);
        String customerSitePath = cms.getSitePath(file);
        ArrayList<I_CmsXmlContentValue> productValues = new ArrayList<>(content.getValues("Product", LOCALE));
        // reverse value list so deleting values does not change the indexes in xpaths
        // for the following iterations
        Collections.reverse(productValues);

        for (I_CmsXmlContentValue prodVal : productValues) {
            String productType = subvalueStr(prodVal, "ProductType");
            String clusterAmount = subvalueStr(prodVal, "ClusterAmount");
            String paid = subvalueStr(prodVal, "Paid");
            String activationDate = subvalueStr(prodVal, "ActivationDate");
            String notes = subvalueStr(prodVal, "Notes");
            String inactive = subvalueStr(prodVal, "DisplaySales");
            if (relationProducts.contains(productType)) {
                List<I_CmsXmlContentValue> relationValues = content.getValues(prodVal.getPath() + "/Relation", LOCALE);
                boolean productErrors = false;
                for (I_CmsXmlContentValue relationVal : relationValues) {
                    String path = relationVal.getStringValue(cms);
                    if (CmsStringUtil.isEmptyOrWhitespaceOnly(path)) {
                        continue;
                    }
                    try {
                        CmsFile productFile = cms.readFile(cms.readResource(path, CmsResourceFilter.IGNORE_EXPIRATION));
                        CmsXmlContent productContent = CmsXmlContentFactory.unmarshal(cms, productFile);
                        setValue(productContent, "CustomerRelation", customerSitePath);
                        setValue(productContent, "ProductType", productType);
                        setValue(productContent, "ClusterAmount", clusterAmount);
                        setValue(productContent, "Paid", paid);
                        setValue(productContent, "ActivationDate", activationDate);
                        prependValue(productContent, "Notes", notes);
                        setValue(productContent, "Inactive", inactive);
                        save(productContent);
                    } catch (CmsVfsResourceNotFoundException e) {
                        LOG.error(
                            "Could not find linked relation from " + file.getRootPath() + " " + e.getLocalizedMessage(),
                            e);
                        productErrors = true;
                    }

                }
                if ((relationValues.size() == 0) || productErrors) {
                    LOG.error(
                        "Not changing product nested content in " + customerSitePath + " because of previous errors");
                    continue;
                } else {
                    content.removeValue(prodVal.getPath(), LOCALE, CmsXmlUtils.getXpathIndexInt(prodVal.getPath()) - 1);
                }
            } else if (noRelationProducts.contains(productType)) {
                List<String> fieldsToDelete = Arrays.asList("Relation", "Relation"); // relation can occur twice
                for (String field : fieldsToDelete) {
                    String path = prodVal.getPath() + "/" + field;
                    if (content.hasValue(path, LOCALE)) {
                        content.removeValue(path, LOCALE, 0);
                    }
                }
            } else {
                LOG.error("Skipping product type '" + productType + "' in " + customer.getRootPath());
            }
            save(content);
        }
    }

    public void run() throws Exception {

        CmsObject cms = m_cms;
        List<CmsResource> customerFiles = cms.getFilesInFolder(m_customerDir, CmsResourceFilter.IGNORE_EXPIRATION);
        for (CmsResource customerFile : customerFiles) {
            processCustomerFile(customerFile);
        }
    }

    public void setCustomerDirectory(String directory) {

        m_customerDir = directory;

    }

    private void prependValue(CmsXmlContent content, String name, String fieldValue) {

        if (fieldValue == null) {
            return;
        }
        CmsObject cms = m_cms;
        I_CmsXmlContentValue value = content.getValue(name, LOCALE);
        if (value == null) {
            try {
                value = content.addValue(cms, name, LOCALE, 0);
            } catch (CmsIllegalArgumentException e) {
                return;
            }
        }
        value.setStringValue(cms, fieldValue + value.getStringValue(cms));
    }

    private void save(CmsXmlContent content) throws CmsException {

        if (!SAVE_ENABLED) {
            return;
        }
        byte[] data = content.marshal();
        content.getFile().setContents(data);
        try (AutoCloseable ac = CmsLockUtil.withLockedResources(m_cms, content.getFile())) {
            m_cms.writeFile(content.getFile());
        } catch (Exception e) {
            if (e instanceof CmsException) {
                throw (CmsException)e;
            }
        }
    }

    private void setValue(CmsXmlContent content, String name, String fieldValue) {

        if (fieldValue == null) {
            return;
        }
        CmsObject cms = m_cms;
        I_CmsXmlContentValue value = content.getValue(name, LOCALE);
        if (value == null) {
            try {
                value = content.addValue(cms, name, LOCALE, 0);
            } catch (CmsIllegalArgumentException e) {
                return;
            }
        }
        value.setStringValue(cms, fieldValue);
    }

    private String subvalueStr(I_CmsXmlContentValue val, String key) {

        CmsXmlContent content = (CmsXmlContent)val.getDocument();
        I_CmsXmlContentValue subValue = content.getValue(val.getPath() + "/" + key, val.getLocale());
        if (subValue == null) {
            return null;
        }
        return subValue.getStringValue(m_cms);

    }

}
