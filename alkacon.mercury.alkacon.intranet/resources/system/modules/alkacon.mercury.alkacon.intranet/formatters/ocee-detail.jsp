<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<intranet:check-user>
<mercury:init-messages>
<cms:formatter var="content" val="value">

<fmt:setLocale value="${cms.workplaceLocale}" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">

<intranet:product-vars product="${content}">

<mercury:nl />
<div class="element type-intra-product type-intra-product-ocee"><%----%>

    <mercury:nl />
    <h1 class="product-title"><%----%>
        <fmt:message key="product.name.short.${value.ProductType}" />
        <c:if test="${isV1Combination}">
            ${isOcee ? '- OCEE Lizenz' : (isPremiumSpport ? '- Premium Support' : '- Incident Support' )}
        </c:if>
    </h1><%----%>

    <c:set var="customer" value="${content.value.CustomerRelation.toResource.toXml}" />
    <intranet:customer-display customer="${customer}" addLink="${true}" showNotes="${false}" />

    <c:set var="keys" value="${content.valueList['Key']}" />
    <c:forEach var="key" items="${keys}" varStatus="status">
        <c:if test="${key.value.Distribution.isSet}">
            <c:set var="hasKeyV1" value="${fn:contains(key.value.Distribution.toString, 'oceeV1')}" />
            <c:set var="hasKeyV2" value="${not hasKeyV1}" />
            <c:set var="keyTypeWarning" value="${(hasKeyV1 and isKeyTypeV2) or (hasKeyV2 and isKeyTypeV1)}" />
        </c:if>
    </c:forEach>

    <intranet:product-display product="${content}" addLink="${false}" showKeyTypeWarning="${keyTypeWarning}" />

    <intranet:mail-buttons-accordion product="${content}" />

    <c:if test="${not isInactive and not keyTypeWarning}">
        <div class="intra-btn-block intra-btn-block-addoceekey"><%----%>
            <c:choose>
                <c:when test="${value.KeyType.toString eq 'ocee-v2'}">
                    <div class="intra-btn"><%----%>
                        <form action="${cms.functionDetail['Create OCEE key V2']}"><%----%>
                            <input type="submit" class="btn" value="OCEE V2 Schlüssel erstellen"><%----%>
                            <input type="hidden" name="productfilename" value="${content.resource.sitePath}"><%----%>
                        </form>
                    </div><%----%>
                    <mercury:nl />
                </c:when>
                <c:otherwise>
                    <div class="intra-btn"><%----%>
                        <form action="${cms.functionDetail['Create OCEE key V1']}"><%----%>
                            <input type="submit" class="btn" value="OCEE V1 Schlüssel (alt) erstellen"><%----%>
                            <input type="hidden" name="productfilename" value="${content.resource.sitePath}"><%----%>
                        </form><%----%>
                    </div><%----%>
                    <mercury:nl />
                </c:otherwise>
            </c:choose>
        </div><%----%>
        <mercury:nl />
    </c:if>

    <c:if test="${not empty keys}">
        <div class="subelement intra-keys"><%----%>
            <intranet:sort-keys keys="${keys}">

                <h2 class="intra-heading intra-ocee-keys">OCEE Schlüssel<c:if test="${not empty sortedKeysValid}"> (${sortedKeysValid.size()})</c:if>:</h2><%----%>

                <c:if test="${not empty sortedKeysValid}">
                    <div class="subelement intra-keys-valid"><%----%>
                        <c:forEach var="key" items="${sortedKeysValid}" varStatus="status">
                            <c:set var="hasKeyV1" value="${fn:contains(key.value.Distribution.toString, 'oceeV1')}" />
                            <c:set var="hasKeyV2" value="${not hasKeyV1}" />
                            <c:set var="keyTypeWarning" value="${(hasKeyV1 and isKeyTypeV2) or (hasKeyV2 and isKeyTypeV1)}" />
                            <intranet:ocee-key-display key="${key}" product="${hasKeyV2 ? content : null}" showKeyTypeWarning="${keyTypeWarning}" />
                        </c:forEach>
                    </div><%----%>
                    <mercury:nl />
                </c:if>

                <c:if test="${not empty sortedKeysInvalid}">
                    <button class="btn btn-toggle mb-20" type="button" data-bs-toggle="collapse" data-bs-target="#invalidKeys" aria-expanded="false" aria-controls="invalidKeys"><%----%>
                        Inaktive OCEE Schlüssel (${sortedKeysInvalid.size()})<%----%>
                    </button><%----%>
                    <div class="collapse intra-keys-invalid" id="invalidKeys"><%----%>
                        <c:forEach var="key" items="${sortedKeysInvalid}" varStatus="status">
                            <c:set var="hasKeyV1" value="${fn:contains(key.value.Distribution.toString, 'oceeV1')}" />
                            <c:set var="hasKeyV2" value="${not hasKeyV1}" />
                            <c:set var="keyTypeWarning" value="${(hasKeyV1 and isKeyTypeV2) or (hasKeyV2 and isKeyTypeV1)}" />
                            <intranet:ocee-key-display key="${key}" showKeyTypeWarning="${keyTypeWarning}" css="product-box status-inactive" />
                        </c:forEach>
                    </div><%----%>
                    <mercury:nl />
                </c:if>

            </intranet:sort-keys>
        </div>
    </c:if>

    <c:set var="contacts" value="${content.valueList['Contact']}" />
    <c:if test="${not empty contacts}">
        <div class="subelement intra-contacts"><%----%>
            <h2>Kontaktpersonen für dieses Produkt:</h2><%----%>
            <c:forEach var="contact" items="${contacts}" varStatus="status">
                <intranet:contact-display contact="${contact}" />
            </c:forEach>
        </div><%----%>
        <mercury:nl />
    </c:if>

</div><%----%>
<mercury:nl />

</intranet:product-vars>

</cms:bundle>
</cms:formatter>

</mercury:init-messages>
</intranet:check-user>

