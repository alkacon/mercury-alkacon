<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<intranet:check-user>
<cms:formatter var="content" val="value">

<c:set var="isActive" value="${not value.Inactive.toBoolean}" />

<mercury:nl />
<a href="${content.fileLink}" class="intra-table product-box ${isActive ? '' : 'status-inactive'}"><%----%>
    <mercury:nl />

    <div class="tr">
        <div class="td customer-name"><%----%>
            ${value.Name}
        </div><%----%>
        <div class="td customer-country"><%----%>
            ${value.Country}
        </div><%----%>
    </div>

</a><%----%>

<mercury:nl />

</cms:formatter>
</intranet:check-user>
