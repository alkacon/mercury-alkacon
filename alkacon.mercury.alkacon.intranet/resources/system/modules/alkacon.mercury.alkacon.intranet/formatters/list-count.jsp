<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<intranet:check-user>
<cms:formatter var="content" val="value" locale="en">

    <%-- We just want to load metadata here, no actual results, so the count is 0 --%>
    <mercury:list-search config="${content}" count="0" />

    <mercury:nl />
    <div class="type-intra-list-header"><%----%>
        <h3>${cms.element.setting.heading.isSet ? cms.element.setting.heading.toString : content.value.Title.toString}: ${search.numFound}</h3><%----%>
    </div><%----%>
    <mercury:nl />

</cms:formatter>
</intranet:check-user>