<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<intranet:check-user>
<cms:formatter var="content" val="value">

<fmt:setLocale value="de" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">

<c:set var="customer" value="${value.CustomerRelation.isSet ? value.CustomerRelation.toResource.toXml : null}" />
<c:set var="isActive" value="${not value.Inactive.toBoolean}" />

<intranet:product-vars product="${content}">

<mercury:nl />
<a href="${content.fileLink}" class="intra-table product-box ${productStatus}"><%----%>
    <mercury:nl />

    <c:if test="${not empty customer}">
        <div class="tr"><%----%>
            <div class="td customer-name"><%----%>
                ${customer.value.Name}
            </div><%----%>
            <div class="td customer-country"><%----%>
                ${customer.value.Country}
            </div><%----%>
        </div><%----%>
    </c:if>

    <div class="tr"><%----%>
        <div class="td product-type"><%----%>
            <fmt:message key="product.name.short.${value.ProductType}" />
            <c:if test="${isV1Combination}">
                ${isOcee ? ' - OCEE Schlüssel' : ' - Support'}
            </c:if>
            <c:if test="${empty customer}"> - ${content.resource.name}</c:if>
        </div><%----%>
        <div class="td product-daterange"><%----%>
            ${activeDateRange.formatShort}${expirationGuess ? '* ' : ''}
        </div><%----%>
    </div>

    <c:if test="${isOcee}">
        <div class="tr"><%----%>
            <div class="td product-keyname"><%----%>
                Schlüssel-Name: ${value.KeyName} (${value.KeyType eq 'ocee-v1' ? 'v1' : 'v2'})
            </div><%----%>
        </div>
    </c:if>

</a><%----%>
<mercury:nl />

</intranet:product-vars>
</cms:bundle>

</cms:formatter>
</intranet:check-user>