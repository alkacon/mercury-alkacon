<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<intranet:check-user>
<mercury:init-messages>
<cms:formatter var="content" val="value">

<fmt:setLocale value="${cms.workplaceLocale}" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">
<intranet:product-vars product="${content}">

<mercury:nl />
<div class="element type-intra-product type-intra-product-premium"><%----%>

    <mercury:nl />
    <h1 class="product-title"><%----%>
        <fmt:message key="product.name.short.${value.ProductType}" />
        <c:if test="${isV1Combination}">
            ${isOcee ? '- OCEE Lizenz' : (isPremiumSpport ? '- Premium Support' : '- Incident Support' )}
        </c:if>
    </h1><%----%>

    <c:set var="customer" value="${value.CustomerRelation.toResource.toXml}" />
    <intranet:customer-display customer="${customer}" addLink="${true}" showNotes="${false}" />

    <intranet:product-display product="${content}" addLink="${false}" />

    <intranet:mail-buttons-accordion product="${content}" />

    <c:set var="contacts" value="${content.valueList['Contact']}" />
    <c:if test="${not empty contacts}">
        <div class="subelement intra-contacts"><%----%>
            <h2>Kontaktpersonen für dieses Produkt:</h2><%----%>
            <c:forEach var="contact" items="${contacts}" varStatus="status">
                <intranet:contact-display contact="${contact}" />
            </c:forEach>
        </div><%----%>
        <mercury:nl />
    </c:if>

</div><%----%>
<mercury:nl />

</intranet:product-vars>
</cms:bundle>
</cms:formatter>

</mercury:init-messages>
</intranet:check-user>