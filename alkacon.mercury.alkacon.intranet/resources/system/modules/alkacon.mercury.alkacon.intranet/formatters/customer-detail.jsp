<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<intranet:check-user>
<mercury:init-messages>
<cms:formatter var="content" val="value">

<mercury:nl />
<div class="element type-intra-customer ${value.Inactive.toBoolean ? 'element-inactive' : ''}"><%----%>

    <mercury:nl />
    <intranet:customer-display customer="${content}" addLink="${false}" showNotes="${true}" />

    <c:set var="contacts" value="${content.valueList['Contact']}" />
    <c:if test="${not empty contacts}">
        <div class="subelement intra-contacts"><%----%>
            <h2 class="intra-heading intra-customer-contacts">Kontaktpersonen:</h2><%----%>
            <c:forEach var="contact" items="${contacts}" varStatus="status">
                <intranet:contact-display contact="${contact}" />
            </c:forEach>
        </div><%----%>
        <mercury:nl />
    </c:if>

    <c:if test="${not value.Inactive.toBoolean}">
        <div class="intra-btn-block intra-btn-block-addproduct"><%----%>
            <div class="intra-btn no-edit-point"><%----%>
                <cms:edit
                    create="true"
                    createType="supportOcee"
                    postCreateHandler="alkacon.mercury.alkacon.intranet.CmsIntranetProductPostCreateHandler|${content.resource.sitePath}#${value.Name}">
                    <div class="btn" onclick="this.previousElementSibling.cmsOnClickNew()">OCEE hinzufügen</div><%----%>
                </cms:edit>
            </div><%----%>

            <div class="intra-btn no-edit-point"><%----%>
                <cms:edit
                    create="true"
                    createType="supportPremium"
                    postCreateHandler="alkacon.mercury.alkacon.intranet.CmsIntranetProductPostCreateHandler|${content.resource.sitePath}#${value.Name}">
                    <div class="btn" onclick="this.previousElementSibling.cmsOnClickNew()">Premium Support hinzufügen</div><%----%>
                </cms:edit>
            </div><%----%>

            <div class="intra-btn no-edit-point"><%----%>
                <cms:edit
                    create="true"
                    createType="supportIncident"
                    postCreateHandler="alkacon.mercury.alkacon.intranet.CmsIntranetProductPostCreateHandler|${content.resource.sitePath}#${value.Name}">
                    <div class="btn" onclick="this.previousElementSibling.cmsOnClickNew()">Incident Support hinzufügen</div><%----%>
                </cms:edit>
            </div><%----%>
        </div><%----%>
        <mercury:nl />
    </c:if>

    <c:set var="products" value="${content.resource.incomingRelations}" />
    <c:set var="legacyProducts" value="${content.valueList['Product']}" />

    <c:if test="${not empty products or not empty legacyProducts}">

    <div class="subelement intra-products"><%----%>

        <c:if test="${not empty products}">
            <intranet:sort-products products="${products}">
                <h2 class="intra-heading intra-customer-products">Produkte<c:if test="${not empty sortedProductsValid}"> (${sortedProductsValid.size()})</c:if>:</h2><%----%>

                <c:if test="${not empty sortedProductsValid}">
                    <div class="intra-products-valid"><%----%>
                        <c:forEach var="productFile" items="${sortedProductsValid}" varStatus="status">
                            <mercury:display file="${productFile.sitePath}" editable="${true}" deleteable="${true}" formatter="/system/modules/alkacon.mercury.alkacon.intranet/formatters/product-display-extended.xml" />
                        </c:forEach>
                    </div><%----%>
                    <mercury:nl />
                </c:if>

                <c:if test="${not empty sortedProductsInvalid}">
                    <button class="btn btn-toggle mb-20" type="button" data-bs-toggle="collapse" data-bs-target="#invalidProducts" aria-expanded="false" aria-controls="invalidProducts"><%----%>
                        Inaktive Produkte (${sortedProductsInvalid.size()})<%----%>
                    </button><%----%>
                    <div class="collapse intra-products-invalid" id="invalidProducts"><%----%>
                        <c:forEach var="productFile" items="${sortedProductsInvalid}" varStatus="status">
                            <mercury:display file="${productFile.sitePath}" editable="${true}" deleteable="${true}" formatter="/system/modules/alkacon.mercury.alkacon.intranet/formatters/product-display-extended.xml" />
                        </c:forEach>
                    </div><%----%>
                    <mercury:nl />
                </c:if>

            </intranet:sort-products>
        </c:if>

        <c:if test="${not empty legacyProducts}">
            <intranet:sort-products products="${legacyProducts}">
                <button class="btn btn-toggle mb-20" type="button" data-bs-toggle="collapse" data-bs-target="#legacyProducts" aria-expanded="false" aria-controls="invalidProducts"><%----%>
                    Alte Produkte UP12 / 1ACNL (${sortedProducts.size()})<%----%>
                </button><%----%>
                <div class="collapse" id="legacyProducts"><%----%>
                    <c:forEach var="product" items="${sortedProducts}" varStatus="status">
                        <intranet:product-display product="${product}" showInactiveWarning="${false}" />
                    </c:forEach>
                </div><%----%>
                <mercury:nl />
            </intranet:sort-products>
        </c:if>

    </div><%----%>
    <mercury:nl />

    </c:if>

</div><%----%>
<mercury:nl />

</cms:formatter>
</mercury:init-messages>
</intranet:check-user>