<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<cms:secureparams allowXml="mail-body" />

<intranet:check-user>
<intranet:send-mail>

<mercury:nl/>
<div class="element intra-mail intra-mail-product"><%----%>
    <mercury:nl/>

    <c:if test="${not isMailConfigured}">
        <mercury:alert-online>
            <jsp:attribute name="head">E-Mail Server ist nicht konfiguriert!</jsp:attribute>
        </mercury:alert-online>
    </c:if>

    <c:set var="displayForm" value="${(not isMailSend) and (not empty param.product)}" />
    <c:choose>
        <c:when test="${displayForm}">

            <c:set var="product" value="${cms.wrap[param.product].toResource.toXml}" />
            <c:set var="customer" value="${product.value.CustomerRelation.toResource.toXml}" />
            <c:set var="selectedMail" value="${param.selectedMail}" />
            <c:set var="selectedLocale" value="${param.selectedLocale}" />

            <intranet:mail-vars locale="${selectedLocale}" customer="${customer}" product="${product}">

                <c:set var="mail" value="${mailConfig.localeValueList[selectedLocale]['Mail'].get(selectedMail)}" />

                <c:choose>
                    <c:when test="${fn:containsIgnoreCase(mail.value.ShowIn.toString, 'sales')}">
                        <c:set var="mailFromEmail" value="m.alger@alkacon.com" />
                        <c:set var="mailCCEmail" value="" />
                        <c:set var="mailBCCEmail" value="a.kandzior@alkacon.com" />
                        <c:set var="mailFromName" value="Martina Alger" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="mailFromEmail" value="opencms-support@alkacon.com" />
                        <c:set var="mailCCEmail" value="opencms-support@alkacon.com" />
                        <c:set var="mailBCCEmail" value="" />
                        <c:set var="mailFromName" value="Alkacon OpenCms Support" />
                    </c:otherwise>
                </c:choose>

                <intranet:customer-display customer="${customer}" addLink="${true}" showNotes="${false}" />
                <intranet:product-display product="${product}" addLink="${true}" />

                <form id="product-email" class="styled-form compact intra-form" method="post"><%----%>
                    <mercury:nl/>
                    <fieldset><%----%>

                        <section class="compact compact-cols"><%----%>
                            <label for="input-to" class="label"><%----%>
                                <c:out value="Von:" />
                            </label><%----%>
                            <div class="input input-col-1"><%----%>
                                <input type="email" id="input-from-email" name="mail-from-email" value="${mailFromEmail}" required" ><%----%>
                            </div><%----%>
                            <div class="input input-col-2"><%----%>
                                <input type="text" id="input-from-name" name="mail-from-name" value="${mailFromName}" required" ><%----%>
                            </div><%----%>
                        </section><%----%>
                        <mercury:nl/>

                        <section class="compact"><%----%>
                            <label for="input-to" class="label"><%----%>
                                <c:out value="An:" />
                            </label><%----%>
                            <div class="input"><%----%>
                                <input type="email" id="input-to" name="mail-to" value="${mailRecipients}" required multiple pattern="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},*[\W]*)+$" ><%----%>
                            </div><%----%>
                        </section><%----%>
                        <mercury:nl/>

                        <section class="compact"><%----%>
                            <label for="input-cc" class="label"><%----%>
                                <c:out value="CC:" />
                            </label><%----%>
                            <div class="input"><%----%>
                                <input type="email" id="input-cc" name="mail-cc" value="${mailCCEmail}" multiple pattern="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},*[\W]*)+$" ><%----%>
                            </div><%----%>
                        </section><%----%>
                        <mercury:nl/>

                        <section class="compact"><%----%>
                            <label for="input-bcc" class="label"><%----%>
                                <c:out value="BCC:" />
                            </label><%----%>
                            <div class="input"><%----%>
                                <input type="email" id="input-bcc" name="mail-bcc" value="${mailBCCEmail}" multiple pattern="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},*[\W]*)+$" ><%----%>
                            </div><%----%>
                        </section><%----%>
                        <mercury:nl/>

                        <section class="compact"><%----%>
                            <label for="input-subject" class="label"><%----%>
                                <c:out value="Betreff:" />
                            </label><%----%>
                            <div class="input"><%----%>
                                <input type="text" id="input-subject" name="mail-subject" value="${macros.resolveMacros(mail.value.Subject)}" required><%----%>
                            </div><%----%>
                        </section><%----%>
                        <mercury:nl/>

                        <section><%----%>
                            <div class="textarea"><%----%>
                                <textarea rows="75" id="textarea-body" name="mail-body"><%----%>
                                    ${macros.resolveMacros(mail.value.MailBody)}
                                </textarea><%----%>
                            </div><%----%>
                        </section><%----%>
                        <mercury:nl/>

                        <section><%----%>
                            <div class="from-btn"><%----%>
                                <input type="submit" value="Absenden" id="from-submit-btn" class="btn btn-submit"><%----%>
                                <a href="${product.fileLink}" class="btn btn-cancel">Abbrechen (Zurück)</a><%----%>
                            </div><%----%>
                        </section><%----%>
                        <mercury:nl/>

                    </fieldset><%----%>

                    <input type="hidden" name="product" value="${param.product}" />

                </form><%----%>
                <mercury:nl/>

            </intranet:mail-vars>

        </c:when>

        <c:when test="${isMailSend and (sendMailReturnCode == 0) and (not empty param.product)}">
            <h1>E-Mail wird gesendet...</h1>
            <script>mercury.ready(function($, DEBUG) { window.location.href="${cms.wrap[param.product].toLink}"; });</script>
        </c:when>

        <c:otherwise>

            <div class="form-error"><%----%>
                <h1>${cms.title}</h1>
                <c:if test="${not isMailSend and empty param.product}">
                    <mercury:alert-online>
                        <jsp:attribute name="head">Fehler: Produkt Parameter nicht vorhanden!</jsp:attribute>
                    </mercury:alert-online>
                </c:if>
                <c:if test="${isMailSend and (sendMailReturnCode != 0)}">
                    <mercury:alert-online>
                        <jsp:attribute name="head">Fehler: Versenden der E-Mail fehlgeschlagen!</jsp:attribute>
                    </mercury:alert-online>
                </c:if>
            </div><%----%>

        </c:otherwise>
    </c:choose>

</div><%----%>
<mercury:nl/>

</intranet:send-mail>
</intranet:check-user>