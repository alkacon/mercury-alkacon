<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<cms:secureparams allowXml="activationcode" />

<intranet:check-user>
<intranet:ocee-key-action keyVersion="v1" pageContext="${pageContext}">

<fmt:setLocale value="de" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">

<mercury:nl/>
<div class="element type-intra-keygen type-intra-keygen-v1"><%----%>

<c:choose>
    <c:when test="${not empty action.javascriptLocation}">
        <h1>Schlüssel wird erstellt...</h1>
        <script>mercury.ready(function($, DEBUG) { window.location.href="${action.javascriptLocation}"; });</script>
    </c:when>

    <c:when test="${cms.requestContext.currentProject.onlineProject}">
        <mercury:alert-online >
            <jsp:attribute name="head">Sie befinden sich im Online-Projekt!</jsp:attribute>
            <jsp:attribute name="text">Bitte in das Offline-Projekt wechseln, um einen Schlüssel zu erzeugen.</jsp:attribute>
        </mercury:alert-online>
    </c:when>

    <c:otherwise>
        <c:if test="${not empty action.error}">
            <mercury:alert-online>
                <jsp:attribute name="head">Fehler: ${action.error}</jsp:attribute>
            <jsp:attribute name="text">Bitte korrigieren Sie die Eingaben.</jsp:attribute>
            </mercury:alert-online>
        </c:if>

        <h1>OCEE V1 Schlüssel erstellen für</h1><%----%>

        <c:set var="product" value="${cms.wrap[param.productfilename].toResource.toXml}" />
        <c:set var="customer" value="${product.value.CustomerRelation.toResource.toXml}" />

        <intranet:customer-display customer="${customer}" addLink="${true}" showNotes="${false}" />

        <intranet:product-display product="${product}" addLink="${true}" />

        <intranet:product-vars product="${product}">

            <c:set var="customername"           value="${empty param.customername ? product.value.KeyName.toString : param.customername}" />
            <c:set var="productname"            value="${product.value.ProductType.toString}" />

            <jsp:useBean id="todayPlus31Days"   class="java.util.Date" />
            <c:set var="ignore"                 value="${todayPlus31Days.setTime(todayPlus31Days.time + (31 * 24 * 60 * 60 * 1000))}" />
            <c:set var="todayPlus31DaysStr"><fmt:formatDate value="${todayPlus31Days}" pattern="yyyy-MM-dd" /></c:set>
            <c:set var="validtill"              value="${empty param.validtill ? todayPlus31DaysStr : param.validtill}" />

<script><%----%>
var $form;
var todayPlus31DaysStr = "${todayPlus31DaysStr}";

function init($) {
    $form = $('#ocee-key-form');
    $keytype = $form.find('#keytype');
    $validtillSection = $form.find('#validtillSection');
    $validtill = $validtillSection.find('#validtill');
    $activationcodeSection = $form.find('#activationcodeSection');
    $activationcode = $activationcodeSection.find('#activationcode');
    $developmenthoursSection = $form.find('#developmenthoursSection');
    $developmenthours = $developmenthoursSection.find('#developmenthours');

    enableFields(false, false, false);
}

function typeChange() {
    var idx = $keytype.prop('selectedIndex');
    if (idx == 1) {
        // evaluation
        enableFields(true, false, false);
    } else if (idx == 2) {
        // provisional
        enableFields(true, false, false);
    } else if (idx == 3) {
        // devlopment
        enableFields(false, false, true);
    } else if (idx == 4) {
        // production
        enableFields(false, true, false);
    } else {
        // no / invalid selection
        enableFields(false, false, false);
    }
}

function enableFields(validtill, activationcode, developmenthours) {
    if (validtill) {
        $validtillSection.show();
    } else {
        $validtillSection.hide();
    }
    if (activationcode) {
        $activationcodeSection.show();
        $activationcode.attr('required','required');
    } else {
        $activationcodeSection.hide();
        $activationcode.removeAttr('required');
    }
    if (developmenthours) {
        $developmenthoursSection.show();
        $developmenthours.attr('required','required');
    } else {
        $developmenthoursSection.hide();
        $developmenthours.removeAttr('required');
    }
}

mercury.ready(function($, DEBUG) {
    init($);
});
</script><%----%>


            <form id="ocee-key-form" method="POST" action="" class="styled-form compact intra-form"><%----%>
                <mercury:nl/>

                <fieldset><%----%>
                    <section><%----%>
                        <div class="form-row" ><%----%>
                            <label for="customername" class="label">Schlüssel Name:</label><%----%>
                            <div class="input"><%----%>
                                <input type="text" id="customername" name="customername" value="${customername}" required><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section><%----%>
                       <div class="form-row" ><%----%>
                            <label for="productname" class="label">Distributionstyp:</label><%----%>
                            <div class="select"><%----%>
                                <select id="productname" name="productname" required><%----%>
                                    <option value="1sl" ${isV1Enhancement ? 'selected' : ''}>Server Enhancement Paket (1SL)</option><%----%>
                                    <option value="2cnl" ${isV1Enhancement ? '' : 'selected'}>Cluster Paket (2CNL)</option><%----%>
                                </select><%----%>
                                <i></i><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section>

                    <section><%----%>
                        <div class="form-row" ><%----%>
                            <label for="keytype" class="label">Schlüssel Typ:</label><%----%>
                            <div class="select"><%----%>
                                <select id="keytype" name="keytype" onchange="typeChange();" required><%----%>
                                    <option value="">Bitte auswählen...</option><%----%>
                                    <option value="EVAL">Evaluation</option><%----%>
                                    <option value="PROV">Provisional</option><%----%>
                                    <option value="DEV">Development</option><%----%>
                                    <option value="FULL">Production</option><%----%>
                                </select><%----%>
                                <i></i><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section id="validtillSection"><%----%>
                        <div class="form-row"><%----%>
                            <label for="validtill" class="label">Gültig bis (nur für Evaluation / Provisional):</label><%----%>
                            <div class="input"><%----%>
                                <input type="date" id="validtill" name="validtill" value="${validtill}" ><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section id="developmenthoursSection"><%----%>
                        <div class="form-row"><%----%>
                            <label for="developmenthours" class="label">Stunden für Development:</label><%----%>
                            <div class="select"><%----%>
                                <select id="developmenthours" name="developmenthours"><%----%>
                                    <option value="1">4 Stunden</option><%----%>
                                    <option value="2">8 Stunden</option><%----%>
                                    <option value="3" selected>12 Stunden</option><%----%>
                                    <option value="4">16 Stunden</option><%----%>
                                    <option value="5">20 Stunden</option><%----%>
                                    <option value="6">24 Stunden</option><%----%>
                                    <option value="7">28 Stunden</option><%----%>
                                    <option value="8">32 Stunden</option><%----%>
                                    <option value="9">36 Stunden</option><%----%>
                                </select><%----%>
                                <i></i><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section id="activationcodeSection"><%----%>
                        <div class="form-row" ><%----%>
                            <label for="activationcode" class="label">Activation Code (nur für Production):</label><%----%>
                            <div class="textarea"><%----%>
                                <textarea rows="3" name="activationcode" id="activationcode">${param.activationcode}</textarea><%----%>
                            </div><%----%>
                         </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section><%----%>
                        <div class="from-btn"><%----%>
                            <input type="submit" name="submit" value="V1 Schlüssel erstellen" class="btn btn-submit"><%----%>
                            <a href="${product.fileLink}" class="btn btn-cancel">Abbrechen (Zurück)</a><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                </fieldset><%----%>

                <input type="hidden" name="customerfilename" value="${param.customerfilename}"><%----%>
                <input type="hidden" name="productfilename" value="${param.productfilename}"><%----%>
                <input type="hidden" name="formsubmit" value="true"><%----%>

            </form><%----%>
            <mercury:nl/>

        </intranet:product-vars>
    </c:otherwise>
</c:choose>

</div><%----%>
<mercury:nl/>

</cms:bundle>

</intranet:ocee-key-action>
</intranet:check-user>