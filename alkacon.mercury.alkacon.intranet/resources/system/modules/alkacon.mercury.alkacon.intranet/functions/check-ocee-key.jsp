<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<cms:secureparams allowXml="oceekey" />

<intranet:check-user>

<c:set var="oceekey" value="${fn:trim(param.oceekey)}" />

<mercury:nl/>
<div class="element type-intra-keycheck type-intra-keycheck-v2"><%----%>
    <mercury:nl/>
    <intranet:ocee-key-vars key="${oceekey}">

    <c:if test="${not empty keyException}">
        <mercury:alert-online>
            <jsp:attribute name="head">Schlüssel konnte nicht gelesen werden!</jsp:attribute>
            <jsp:attribute name="text">
                <c:out value="${keyException.toString()}" />
            </jsp:attribute>
        </mercury:alert-online>
    </c:if>

    <c:choose>
        <c:when test="${empty oceekey or not empty keyException}">
            <%-- Display key input form --%>

            <form id="ocee-key-check-form" method="POST" action="" class="styled-form compact intra-form"><%----%>
                <mercury:nl/>

                <fieldset><%----%>

                    <section><%----%>
                        <div class="form-row" ><%----%>
                            <label for="oceekey" class="label">Zu prüfender OCEE Schlüssel:</label><%----%>
                            <div class="textarea"><%----%>
                                <textarea rows="5" name="oceekey" id="oceekey">${oceekey}</textarea><%----%>
                            </div><%----%>
                         </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section><%----%>
                        <div class="from-btn"><%----%>
                            <input type="submit" name="submit" value="Schlüssel prüfen" class="btn btn-submit"><%----%>
                            <div class="btn btn-cancel" onclick="history.back()">Abbrechen (Zurück)</div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                </fieldset><%----%>

            </form><%----%>
            <mercury:nl/>
        </c:when>

        <c:when test="${keyType eq 'v1'}">
            <h2>Dies ist (vermutlich) ein OCEE V1 Schlüssel</h2><%----%>
            <h3>Ausgelesene Daten:</h3><%----%>
            <mercury:nl />

            <div class="intra-table loose intra-block intra-block-ocee-keys"><%----%>

                <div class="tr ocee-key-licence"><%----%>
                    <div class="td td-label">Schlüssel:</div><%----%>
                    <div class="td td-content"><%----%>
                        ${oceekey}
                    </div><%----%>
                </div><%----%>

                <c:if test="${not empty keyInfo.licenseDate}">
                    <div class="tr ocee-key-date"><%----%>
                        <div class="td td-label">Lizenzdatum:</div><%----%>
                        <div class="td td-content"><%----%>
                            <c:set var="instanceDate" value="${cms.wrap[keyInfo.licenseDate].toInstanceDate}" />
                            <c:set var="ignore" value="${instanceDate.setWholeDay(true)}" />
                            ${instanceDate.formatShort}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${not empty keyInfo.expirationDate}">
                    <div class="tr ocee-key-expiration"><%----%>
                        <div class="td td-label">Ablaufdatum:</div><%----%>
                        <div class="td td-content"><%----%>
                            <c:set var="instanceDate" value="${cms.wrap[keyInfo.expirationDate].toInstanceDate}" />
                            <c:set var="ignore" value="${instanceDate.setWholeDay(true)}" />
                            ${instanceDate.formatShort}
                        </div><%----%>
                    </div><%----%>
                </c:if>

            </div><%----%>
        </c:when>

        <c:otherwise>
            <h2>Dies ist ein OCEE V2 Schlüssel</h2><%----%>
            <h3>Ausgelesene Daten:</h3><%----%>
            <mercury:nl />

            <intranet:ocee-key-display oceekey="${oceekey}" />

        </c:otherwise>
    </c:choose>

    </intranet:ocee-key-vars>
</div><%----%>
<mercury:nl/>

</intranet:check-user>