<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<cms:secureparams allowXml="activationcode" />

<intranet:check-user>
<intranet:ocee-key-action keyVersion="v2" pageContext="${pageContext}">

<fmt:setLocale value="de" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">

<mercury:nl/>
<div class="element type-intra-keygen type-intra-keygen-v2"><%----%>

<c:choose>
    <c:when test="${not empty action.javascriptLocation}">
        <h1>Schlüssel wird erstellt...</h1>
        <script>mercury.ready(function() { window.location.href="${action.javascriptLocation}"; });</script>
    </c:when>

    <c:when test="${cms.requestContext.currentProject.onlineProject}">
        <mercury:alert-online >
            <jsp:attribute name="head">Sie befinden sich im Online-Projekt!</jsp:attribute>
            <jsp:attribute name="text">Bitte in das Offline-Projekt wechseln, um einen Schlüssel zu erzeugen.</jsp:attribute>
        </mercury:alert-online>
    </c:when>

    <c:otherwise>
        <c:if test="${not empty action.error}">
            <mercury:alert-online>
                <jsp:attribute name="head">Fehler: ${action.error}</jsp:attribute>
            <jsp:attribute name="text">Bitte korrigieren Sie die Eingaben.</jsp:attribute>
            </mercury:alert-online>
        </c:if>

        <c:set var="product" value="${cms.wrap[param.productfilename].toResource.toXml}" />
        <c:set var="customer" value="${product.value.CustomerRelation.toResource.toXml}" />

        <intranet:customer-display customer="${customer}" addLink="${true}" showNotes="${false}" />

        <intranet:product-display product="${product}" addLink="${true}" />

        <intranet:product-vars product="${product}">

            <c:set var="customername"           value="${empty param.customername ? product.value.KeyName.toString : param.customername}" />
            <c:set var="clustersize"            value="${empty param.clustersize ? oceeNodesTotal : param.clustersize}" />
            <c:set var="developmenthours"       value="${empty param.developmenthours ? 12 : param.developmenthours}" />
            <c:set var="billingid"              value="${product.value.BillingId.toString}" />

            <c:if test="${empty billingid}">
                <c:set var="hideFormSubmit"     value="${true}" />
                <mercury:alert-online>
                    <jsp:attribute name="head">Fehler: Keine Installations-ID!</jsp:attribute>
                    <jsp:attribute name="text">Bitte in den Produktdaten eine Installations-ID eintragen.</jsp:attribute>
                </mercury:alert-online>
            </c:if>

            <c:set var="expDate"><fmt:formatDate value="${expirationDate.start}" pattern="yyyy-MM-dd" /></c:set>
            <c:set var="validtill"              value="${empty param.validtill ? expDate : param.validtill}" />

            <jsp:useBean id="todayPlus31Days"   class="java.util.Date" />
            <c:set var="ignore"                 value="${todayPlus31Days.setTime(todayPlus31Days.time +( 31 * 24 * 60 * 60 * 1000))}" />
            <c:set var="todayPlus31DaysStr"><fmt:formatDate value="${todayPlus31Days}" pattern="yyyy-MM-dd" /></c:set>

<script><%----%>
var $form;
var expDate = "${expDate}";
var todayPlus31DaysStr = "${todayPlus31DaysStr}";

function init($) {
    $form = $('#ocee-key-form');
    $keytype = $form.find('#keytype');
    $validtillSection = $form.find('#validtillSection');
    $validtill = $validtillSection.find('#validtill');
    $clustersizeSection = $form.find('#clustersizeSection');
    $clustersize = $clustersizeSection.find('#clustersize');
    $activationcodeSection = $form.find('#activationcodeSection');
    $activationcode = $activationcodeSection.find('#activationcode');
    $developmenthoursSection = $form.find('#developmenthoursSection');
    $developmenthours = $developmenthoursSection.find('#developmenthours');

    enableFields(false, false, false, false);
}

function typeChange() {
    var idx = $keytype.prop('selectedIndex');
    if (idx == 1) {
        // evaluation
        enableFields(true, false, false, false);
        $validtill.val(todayPlus31DaysStr);
    } else if (idx == 2) {
        // provisional
        enableFields(true, true, false, false);
        $validtill.val(todayPlus31DaysStr);
    } else if (idx == 3) {
        // devlopment
        enableFields(false, true, false, true);
        $clustersize.val(2);
    } else if (idx == 4) {
        // production
        enableFields(true, true, true, false);
        $validtill.val(expDate);
    } else {
        // no / invalid selection
        enableFields(false, false, false, false);
    }
}

function enableFields(validtill, clustersize, activationcode, developmenthours) {
    if (validtill) {
        $validtillSection.show();
    } else {
        $validtillSection.hide();
    }
    if (clustersize) {
        $clustersize.val(${clustersize});
        $clustersizeSection.show();
    } else {
        $clustersize.val(2);
        $clustersizeSection.hide();
    }
    if (activationcode) {
        $activationcodeSection.show();
        $activationcode.attr('required','required');
    } else {
        $activationcodeSection.hide();
        $activationcode.removeAttr('required');
    }
    if (developmenthours) {
        $developmenthoursSection.show();
    } else {
        $developmenthoursSection.hide();
    }
}

mercury.ready(function($, DEBUG) {
    init($);
});
</script><%----%>

            <h1>OCEE V2 Schlüssel erstellen für</h1><%----%>

            <form id="ocee-key-form" method="POST" action="" class="styled-form compact intra-form"><%----%>
                <mercury:nl/>

                <fieldset><%----%>
                    <section><%----%>
                        <div class="form-row" ><%----%>
                            <label for="customername" class="label">Schlüssel Name:</label><%----%>
                            <div class="input"><%----%>
                                <input type="text" id="customername" name="customername" value="${customername}" required><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section><%----%>
                        <div class="form-row" ><%----%>
                            <label for="keytype" class="label">Schlüssel Typ:</label><%----%>
                            <div class="select"><%----%>
                                <select id="keytype" name="keytype" onchange="typeChange();" required><%----%>
                                    <option value="">Bitte auswählen...</option><%----%>
                                    <option value="evaluation">Evaluation</option><%----%>
                                    <option value="provisional">Provisional</option><%----%>
                                    <option value="development">Development</option><%----%>
                                    <option value="production">Production</option><%----%>
                                </select><%----%>
                                <i></i><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section id="clustersizeSection"><%----%>
                        <div class="form-row"><%----%>
                            <label for="clustersize" class="label">Anzahl Knoten im Cluster:</label><%----%>
                            <div class="input"><%----%>
                                <input type="text" id="clustersize" name="clustersize" value="${clustersize}" required><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section id="validtillSection"><%----%>
                        <div class="form-row"><%----%>
                            <label for="validtill" class="label">Schlüssel Ablaufdatum:</label><%----%>
                            <div class="input"><%----%>
                                <input type="date" id="validtill" name="validtill" value="${validtill}" ><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section id="developmenthoursSection"><%----%>
                        <div class="form-row"><%----%>
                            <label for="developmenthours" class="label">Stunden für Development:</label><%----%>
                            <div class="input"><%----%>
                                <input type="text" id="developmenthours" name="developmenthours" value="${developmenthours}"><%----%>
                            </div><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section id="activationcodeSection"><%----%>
                        <div class="form-row" ><%----%>
                            <label for="activationcode" class="label">Activation Code (nur für Typ "Production"):</label><%----%>
                            <div class="textarea"><%----%>
                                <textarea rows="5" name="activationcode" id="activationcode">${param.activationcode}</textarea><%----%>
                            </div><%----%>
                         </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                    <section><%----%>
                        <div class="from-btn"><%----%>
                            <c:if test="${not hideFormSubmit}">
                                <input type="submit" name="submit" value="V2 Schlüssel erstellen" class="btn btn-submit"><%----%>
                            </c:if>
                            <a href="${product.fileLink}" class="btn btn-cancel">Abbrechen (Zurück)</a><%----%>
                        </div><%----%>
                    </section><%----%>
                    <mercury:nl/>

                </fieldset><%----%>

                <input type="hidden" name="customerfilename" value="${param.customerfilename}"><%----%>
                <input type="hidden" name="productfilename" value="${param.productfilename}"><%----%>
                <input type="hidden" name="billingid" value="${billingid}"><%----%>
                <input type="hidden" name="formsubmit" value="true"><%----%>

            </form><%----%>
            <mercury:nl/>

        </intranet:product-vars>
    </c:otherwise>
</c:choose>

</div><%----%>
<mercury:nl/>

</cms:bundle>

</intranet:ocee-key-action>
</intranet:check-user>