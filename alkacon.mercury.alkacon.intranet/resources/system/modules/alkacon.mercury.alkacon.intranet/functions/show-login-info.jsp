<%@page pageEncoding="UTF-8"
    buffer="none"
    session="false"
    trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<mercury:nl/>
<div class="type-login-info"><%----%>
    <mercury:nl/>

    <div class="float-right"><%----%>
        <c:if test="${cms.requestContext.currentProject.onlineProject}">
            <span class="badge badge-red"><%----%>
                ${cms.requestContext.currentProject.name}
            </span><%----%>
            ${' '}
        </c:if>
        <span class="badge badge-cyan"><%----%>
            ${cms.requestContext.currentUser.fullName}
        </span><%----%>
    </div><%----%>

</div><%----%>
<mercury:nl/>

