<%@ tag pageEncoding="UTF-8"
    display-name="product-display"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays intranet product information." %>


<%@ attribute name="product" type="java.lang.Object" required="true"
    description="The product to display." %>

<%@ attribute name="locale" type="java.lang.String" required="false"
    description="The locale to use in resource bundles. Default is 'de'. The only other valid option is 'en'" %>

<%@ attribute name="addLink" type="java.lang.Boolean" required="false"
    description="Add a link to the product. Default is 'false'." %>

<%@ attribute name="showInactiveWarning" type="java.lang.Boolean" required="false"
    description="Show a warning in case the product has been marked as inactive. Default is 'true'." %>

<%@ attribute name="showKeyTypeWarning" type="java.lang.Boolean" required="false"
    description="Display a warning in case the selected ocee key type (v1/v2) does not match the existing keys." %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:set var="locale"                     value="${(locale eq 'en') ? locale : 'de'}" />
<c:set var="showInactiveWarning"        value="${empty showInactiveWarning ? true : showInactiveWarning}" />

<c:choose>
    <c:when test="${product['class'].simpleName eq 'CmsJspContentAccessBean'}">
        <c:set var="value"              value="${product.value}" />
        <c:set var="link"               value="${addLink ? product.fileLink : null}" />
    </c:when>
    <c:when test="${product['class'].simpleName eq 'CmsJspResourceWrapper'}">
        <c:set var="value"              value="${product.toXml.value}" />
        <c:set var="link"               value="${addLink ? product.resource.link : null}" />
    </c:when>
    <c:when test="${product['class'].simpleName eq 'CmsJspContentAccessValueWrapper'}">
        <c:set var="value"              value="${product.value}" />
    </c:when>
</c:choose>

<fmt:setLocale value="${locale}" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">

<c:set var="isActive" value="${not value.Inactive.toBoolean}" />

<intranet:product-vars product="${product}">

<mercury:nl />
<div class="intra-intro-product"><%----%>

    <c:if test="${showInactiveWarning and value.Inactive.toBoolean}">
        <mercury:alert-online css="intra-inactive">
            <jsp:attribute name="head">Produkt ist deaktiviert!</jsp:attribute>
            <jsp:attribute name="text">
                <div>Die Daten werden nur noch für Archivzwecke aufbewahrt.</div>
                <div>Es wird kein Support mehr für dieses Produkt geleitet!</div>
            </jsp:attribute>
        </mercury:alert-online>
    </c:if>

    <mercury:nl />
    <div class="intra-table loose intra-block intra-block-product product-box ${productStatus}">
        <mercury:nl />

        <div class="tr product-type"><%----%>
                <div class="td td-label">Produkt:</div><%----%>
                <div class="td td-content"><%----%>
                    <mercury:link link="${link}" test="${addLink}" css="box-link">
                        <c:if test="${isV1Combination}">
                            ${isOcee ? 'OCEE Lizenz - ' : (isPremiumSpport ? 'Premium Support - ' : 'Incident Support - ' )}
                        </c:if>
                        <fmt:message key="product.name.short.${value.ProductType}" />
                    </mercury:link>
                </div><%----%>
        </div><%----%>
        <mercury:nl />

        <div class="tr product-daterange"><%----%>
            <div class="td td-label">Gültigkeit:</div><%----%>
            <div class="td td-content"><%----%>
                ${activeDateRange.formatShort}${expirationGuess ? '*' : ''}
            </div><%----%>
        </div><%----%>
        <mercury:nl />

        <c:if test="${isIncidentSpport and not empty value.AccountID}">
            <div class="tr product-accountid"><%----%>
                <div class="td td-label">Kunden-Zugangscode:</div><%----%>
                <div class="td td-content"><%----%>
                    ${value.AccountID}
                </div><%----%>
            </div><%----%>
            <mercury:nl />
        </c:if>

        <c:if test="${isOcee and not empty value.KeyName}">
            <div class="tr product-keyname"><%----%>
                <div class="td td-label">Installations-Name:</div><%----%>
                <div class="td td-content"><%----%>
                    ${value.KeyName}
                </div><%----%>
            </div><%----%>
            <mercury:nl />
        </c:if>

        <c:if test="${isOcee and isKeyTypeV2 and not empty value.BillingId}">
            <div class="tr product-billingid"><%----%>
                <div class="td td-label">Installations-ID:</div><%----%>
                <div class="td td-content"><%----%>
                    ${value.BillingId}
                </div><%----%>
            </div><%----%>
            <mercury:nl />
        </c:if>

        <c:if test="${isOcee and not empty value.KeyType}">
            <div class="tr product-keytype ${showKeyTypeWarning ? 'tr-warning' : ''}"><%----%>
                <div class="td td-label">Schlüssel-Variante:</div><%----%>
                <div class="td td-content"><%----%>
                    <fmt:message key="product.KeyType.${value.KeyType}" />
                    <c:if test="${showKeyTypeWarning}">
                        <br><span class="message">Achtung: Die ausgewählte Schlüssel-Variante entspricht nicht den vorhandenen Schlüsseln!</span>
                    </c:if>
                </div><%----%>
            </div><%----%>
            <mercury:nl />
        </c:if>

        <c:if test="${isOcee and not isV1Enhancement}">
            <div class="tr product-clusteramount"><%----%>
                <div class="td td-label">Knoten im Cluster:</div><%----%>
                <div class="td td-content"><%----%>
                    <c:choose>
                        <c:when test="${oceeNodesAdd > 0}">
                            <span>${oceeNodesTotal} (${oceeNodesBase} aus Grundpaket + ${oceeNodesAdd} zusätzliche Knoten)</span><%----%>
                        </c:when>
                        <c:otherwise>
                            <span>${oceeNodesBase} (aus Grundpaket)</span><%----%>
                        </c:otherwise>
                    </c:choose>
                </div><%----%>
            </div><%----%>
            <mercury:nl />
        </c:if>

        <c:if test="${not empty value.Notes}">
            <div class="tr notes"><%----%>
                <div class="td"><%----%>
                    ${value.Notes}
                </div><%----%>
            </div><%----%>
        </c:if>
        <mercury:nl />

    </div>


    <c:if test="${false}">
        <%-- Debug generated product-vars --%>
        <div class="subelement"><%----%>
            <em><%----%>
                <span>${productType}</span>${' '}<%----%>
                <span>${isExpired ? '- Expired' : '- Active'}</span>${' '}<%----%>
                <span>${isProductV1 ? '- ProductV1 ' : ''}</span><%----%>
                <span>${isProductV2 ? '- ProductV2 ' : ''}</span><%----%>
                <span>${isV1Combination ? '- Combination ' : ''}</span><%----%>
                <span>${isV1Cluster ? '- V1 Cluster ' : ''}</span><%----%>
                <span>${isOcee ? '- OCEE ' : ''}</span><%----%>
                <span>${isPremiumSpport ? '- Premium ' : ''}</span><%----%>
                <span>${isIncidentSpport ? '- Incident ' : ''}</span><%----%>
                <span>- Period: ${activeDateRange.formatShort}${expirationGuess ? ' (* Guessed)' : ''}</span>${' '}<%----%>
            </em><%----%>
        </div><%----%>
    </c:if>

</div><%----%>
<mercury:nl />

</intranet:product-vars>
</cms:bundle>
