<%@ tag pageEncoding="UTF-8"
    display-name="contact-display"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays intranet contact information." %>


<%@ attribute name="contact" type="java.lang.Object" required="true"
    description="The contact to display." %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:set var="value" value="${contact.value}" />

<c:if test="${(not empty value.Name) or (not empty value.Email) or (not empty value.Phone) or (not empty value.Address) or (not empty value.Notes)}">

    <mercury:nl />
    <div class="intra-table loose intra-block intra-block-contacts"><%----%>
        <c:if test="${not empty value.Name}">
            <div class="tr contact-name"><%----%>
                <div class="td td-label">Name:</div><%----%>
                <div class="td td-content"><%----%>
                    <mercury:link link="mailto:${value.Email}" test="${not empty value.Email}">
                        <c:out value="${value.Name}" />
                    </mercury:link>
                </div><%----%>
            </div><%----%>
        </c:if>
        <c:if test="${not empty value.Email}">
            <div class="tr"><%----%>
                <div class="td td-label">E-Mail:</div><%----%>
                <div class="td td-content"><%----%>
                    <mercury:link link="mailto:${value.Email}" test="${not empty value.Email}">
                        <c:out value="${value.Email}" />
                    </mercury:link>
                </div><%----%>
            </div><%----%>
        </c:if>
        <c:if test="${not empty value.Phone}">
            <div class="tr"><%----%>
                <div class="td td-label">Telefon:</div><%----%>
                <div class="td td-content"><c:out value="${value.Phone}" /></div><%----%>
            </div><%----%>
        </c:if>
        <c:if test="${not empty value.Address}">
            <div class="tr"><%----%>
                <div class="td td-label">Adresse:</div><%----%>
                <div class="td td-content">${cms:escapeHtml(value.Address)}</div><%----%>
            </div><%----%>
        </c:if>
        <c:if test="${not empty value.Notes}">
            <div class="tr"><%----%>
                <div class="td td-label">Notizen:</div><%----%>
                <div class="td td-content">${cms:escapeHtml(value.Notes)}</div><%----%>
            </div><%----%>
        </c:if>
    </div><%----%>
    <mercury:nl />

</c:if>