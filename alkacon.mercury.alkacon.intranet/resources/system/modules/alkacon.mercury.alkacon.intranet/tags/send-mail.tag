<%@ tag pageEncoding="UTF-8"
    display-name="product-vars"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    description="Provides convenient access variables for a product." %>


<%@ variable name-given="isMailConfigured"      declare="true" %>
<%@ variable name-given="isMailSend"            declare="true" %>
<%@ variable name-given="sendMailReturnCode"    declare="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<%
   getJspContext().setAttribute(
        "mailhost", org.opencms.main.OpenCms.getSystemInfo().getMailSettings().getDefaultMailHost().getHostname()
   );
%>

<c:set var="isMailSend" value="${not empty param['mail-subject']}" />
<c:set var="isMailConfigured" value="${mailhost != 'my.smtp.server'}" />

<c:if test="${isMailConfigured && isMailSend}">
    <c:catch var="mailException">

        <c:set var="mailFromEmail" value="${param['mail-from-email']}" />
        <c:set var="mailFromName" value="${param['mail-from-name']}" />

        <jsp:useBean id="mail" scope="page" class="org.opencms.mail.CmsSimpleMail" />
        <c:set var="ignore">
            ${mail.setSubject(param['mail-subject'])}
            <c:forTokens var="addr" items="${param['mail-to']}" delims=",">
                ${mail.addTo(addr)}
            </c:forTokens>
            ${mail.setMsg(param['mail-body'])}
            <c:if test="${not empty param['mail-bcc']}">
                <c:forTokens var="addr" items="${param['mail-bcc']}" delims=",">
                    ${mail.addBcc(addr)}
                </c:forTokens>
            </c:if>
            <c:if test="${not empty param['mail-cc']}">
                <c:forTokens var="addr" items="${param['mail-cc']}" delims=",">
                    ${mail.addCc(addr)}
                </c:forTokens>
            </c:if>
            <c:if test="${not empty mailFromEmail}">
                ${mail.setFrom(mailFromEmail, empty mailFromName ? null : mailFromName)}
            </c:if>
            ${mail.send()}
        </c:set>
    </c:catch>
    <c:choose>
        <c:when test="${empty mailException}">
            <c:set var="sendMailReturnCode" value="0" />
        </c:when>
        <c:otherwise>
            <!-- ${mailException} -->
            <c:set var="sendMailReturnCode" value="1" />
        </c:otherwise>
    </c:choose>
</c:if>

<jsp:doBody />
