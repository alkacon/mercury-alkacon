<%@ tag pageEncoding="UTF-8"
    display-name="ocee-key-action"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    import="
        alkacon.mercury.alkacon.intranet.*
    "
    description="Generates an OCEE key." %>


<%@ attribute name="keyVersion" type="java.lang.String" required="true"
    description="OCEE key version to generate. Can be 'v1' or 'v2'." %>

<%@ attribute name="pageContext" type="javax.servlet.jsp.PageContext" required="true"
    description="JSP page context - must be passes to tag since it is required by key generating beans." %>


<%@ variable name-given="action" declare="true" variable-class="java.lang.Object" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<c:choose>
    <c:when test="${keyVersion eq 'v1'}">
        <%
        CmsOldKeyGenActionElement keygen = new CmsOldKeyGenActionElement(pageContext, request, response);
        getJspContext().setAttribute("action", keygen);
        keygen.action();
        %>
    </c:when>
    <c:when test="${keyVersion eq 'v2'}">
        <%
        CmsKeyGenActionElement keygen = new CmsKeyGenActionElement(pageContext, request, response);
        getJspContext().setAttribute("action", keygen);
        keygen.action();
        %>
    </c:when>
</c:choose>

<jsp:doBody />