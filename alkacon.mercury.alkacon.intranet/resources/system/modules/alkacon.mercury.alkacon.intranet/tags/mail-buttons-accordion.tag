<%@ tag pageEncoding="UTF-8"
    display-name="mail-buttons-accordion"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays an accordion of mail buttons ." %>

<%@ attribute name="product" type="java.lang.Object" required="true"
    description="The product to display the mail buttons for." %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:if test="${not product.value.Inactive.toBoolean}">

<mercury:nl />
<div class="subelement accordion-items" id="acco_parent"><%----%>

    <c:set var="open" value="${false}" />

    <c:set var="buttons"><intranet:mail-buttons locale="en" product="${product}" mailtype="product" /></c:set>
    <c:if test="${not empty buttons}">
        <div class="accordion "><%----%>
            <div class="acco-header"><%----%>
                <a class="acco-toggle ${open ? '' : 'collapsed'}" data-bs-toggle="collapse" data-bs-target="#acco_3" href="#acco_3" aria-expanded="${open}"><%----%>
                    <span>Produkt E-Mails (English)</span><%----%>
                </a><%----%>
            </div><%----%>
        </div><%----%>
        <div id="acco_3" class="acco-body collapse ${open ? 'show' : ''}" data-bs-parent="#acco_parent"><%----%>
            ${buttons}
        </div><%----%>
        <mercury:nl />
        <c:set var="open" value="${false}" />
    </c:if>

    <c:set var="buttons"><intranet:mail-buttons locale="de" product="${product}" mailtype="product" /></c:set>
    <c:if test="${not empty buttons}">
        <div class="accordion "><%----%>
            <div class="acco-header"><%----%>
                <a class="acco-toggle ${open ? '' : 'collapsed'}" data-bs-toggle="collapse" data-bs-target="#acco_1" href="#acco_1" aria-expanded="${open}"><%----%>
                    <span>Produkt E-Mails (Deutsch)</span><%----%>
                </a><%----%>
            </div><%----%>
        </div><%----%>
        <div id="acco_1" class="acco-body collapse ${open ? 'show' : ''}" data-bs-parent="#acco_parent"><%----%>
            ${buttons}
        </div><%----%>
        <mercury:nl />
        <c:set var="open" value="${false}" />
    </c:if>

    <c:set var="buttons"><intranet:mail-buttons locale="en" product="${product}" mailtype="sales" /></c:set>
    <c:if test="${not empty buttons}">
        <div class="accordion "><%----%>
            <div class="acco-header"><%----%>
                <a class="acco-toggle ${open ? '' : 'collapsed'}" data-bs-toggle="collapse" data-bs-target="#acco_4" href="#acco_4" aria-expanded="${open}"><%----%>
                    <span>Vertrieb E-Mails (English)</span><%----%>
                </a><%----%>
            </div><%----%>
        </div><%----%>
        <div id="acco_4" class="acco-body collapse ${open ? 'show' : ''}" data-bs-parent="#acco_parent"><%----%>
            ${buttons}
        </div><%----%>
        <mercury:nl />
        <c:set var="open" value="${false}" />
    </c:if>

    <c:set var="buttons"><intranet:mail-buttons locale="de" product="${product}" mailtype="sales" /></c:set>
    <c:if test="${not empty buttons}">
        <div class="accordion "><%----%>
            <div class="acco-header"><%----%>
                <a class="acco-toggle ${open ? '' : 'collapsed'}" data-bs-toggle="collapse" data-bs-target="#acco_2" href="#acco_2" aria-expanded="${open}"><%----%>
                    <span>Vertrieb E-Mails (Deutsch)</span><%----%>
                </a><%----%>
            </div><%----%>
        </div><%----%>
        <div id="acco_2" class="acco-body collapse ${open ? 'show' : ''}" data-bs-parent="#acco_parent"><%----%>
            ${buttons}
        </div><%----%>
        <mercury:nl />
        <c:set var="open" value="${false}" />
    </c:if>

</div><%----%>
<mercury:nl />

</c:if>