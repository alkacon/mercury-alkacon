<%@ tag pageEncoding="UTF-8"
    display-name="format-license-key"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    import="java.util.*"
    description="Formats and prints an OCEE license key with newlines." %>


<%@ attribute name="key" type="java.lang.String" required="true"
    description="The license key string." %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<%
String key = (String) jspContext.getAttribute("key");
key = key.trim().replace("\n","");
StringBuilder buffer = new StringBuilder();
int columns = 60;
int pos = 0;
while (pos < key.length()) {
    int rowLength = Math.min(columns, key.length() - pos);
    buffer.append(key.substring(pos, pos+rowLength));
    pos += rowLength;
    if (pos != key.length()) {
        buffer.append("&#10;");
    }
}
out.println(buffer.toString());
%>