<%@ tag pageEncoding="UTF-8"
    display-name="ocee-key-display"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays an OCEE key." %>


<%@ attribute name="key" type="java.lang.Object" required="false"
    description="The OCEE key XML data to display." %>

<%@ attribute name="oceekey" type="java.lang.Object" required="false"
    description="The OCEE key to display.
    For OCEE V2 keys, this will be read from the XML data if not set." %>

<%@ attribute name="showKeyTypeWarning" type="java.lang.Boolean" required="false"
    description="Display a warning in case this key type (v1/v2) does not match the selected key type in the product." %>

<%@ attribute name="product" type="java.lang.Object" required="false"
    description="The OCEE product this key belongs to.
    Used to check if the key name / installation id matches the product.
    This check is only required for active v2 keys." %>

<%@ attribute name="css" type="java.lang.Object" required="false"
    description="Optional CSS to append to the generated DIV." %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<c:set var="value" value="${not empty key ? key.value : null}" />

<fmt:setLocale value="de" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">

<mercury:nl />
<div class="intra-table loose intra-block intra-block-ocee-keys ${css}"><%----%>

    <c:if test="${empty oceekey and value.LicenceKey.isSet and (value.Distribution.toString eq 'oceeV2Subscription')}">
        <c:set var="oceekey" value="${value.LicenceKey}" />
    </c:if>

    <intranet:ocee-key-vars key="${oceekey}">

        <c:choose>

            <c:when test="${empty keyInfo and not empty value}">

                <c:if test="${value.Name.isSet}">
                    <div class="tr ocee-key-name"><%----%>
                        <div class="td td-label">Name:</div><%----%>
                        <div class="td td-content"><%----%>
                            ${value.Name}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.Distribution.isSet}">
                    <div class="tr ocee-key-distribution ${showKeyTypeWarning ? 'tr-warning' : ''}"><%----%>
                        <div class="td td-label">Variante:</div><%----%>
                        <div class="td td-content"><%----%>
                            <fmt:message key="product.distribution.${value.Distribution}" />
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.Type.isSet}">
                    <div class="tr ocee-key-type"><%----%>
                        <div class="td td-label">Typ:</div><%----%>
                        <div class="td td-content"><%----%>
                            ${value.Type}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.DevelopmentTime.isSet}">
                    <div class="tr ocee-key-development"><%----%>
                        <div class="td td-label">Entwicklungszeit:</div><%----%>
                        <div class="td td-content">${value.DevelopmentTime.toInteger * 4} Stunden</div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.Date.isSet}">
                    <div class="tr ocee-key-date"><%----%>
                        <div class="td td-label">Erzeugt am:</div><%----%>
                        <div class="td td-content"><%----%>
                            <c:set var="instanceDate" value="${value.Date.toInstanceDate}" />
                            <c:set var="ignore" value="${instanceDate.setWholeDay(true)}" />
                            ${instanceDate.formatShort}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.ExpirationDate.isSet}">
                    <div class="tr ocee-key-expiration"><%----%>
                        <div class="td td-label">Ablaufdatum:</div><%----%>
                        <div class="td td-content"><%----%>
                            <c:set var="instanceDate" value="${value.ExpirationDate.toInstanceDate}" />
                            <c:set var="ignore" value="${instanceDate.setWholeDay(true)}" />
                            ${instanceDate.formatShort}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.ActivationKey.isSet}">
                    <div class="tr ocee-key-activation"><%----%>
                        <div class="td td-label">Aktivierungscode:</div><%----%>
                        <div class="td td-content"><%----%>
                            ${value.ActivationKey}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.ClusterSize.isSet}">
                    <div class="tr ocee-key-clustersize"><%----%>
                        <div class="td td-label">Anzahl Knoten:</div><%----%>
                        <div class="td td-content"><%----%>
                            ${value.ClusterSize}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:if test="${value.LicenceKey.isSet}">
                    <div class="tr ocee-key-licence"><%----%>
                        <div class="td td-label">Lizenzschlüssel:</div><%----%>
                        <div class="td td-content"><%----%>
                            ${value.LicenceKey}
                        </div><%----%>
                    </div><%----%>
                </c:if>

            </c:when>

            <c:when test="${not empty keyInfo}">

                <c:set var="showNameWarning" value="${not empty product and (product.value.KeyName.toString ne keyInfo.name)}" />
                <div class="tr ocee-key-name ${showNameWarning ? 'tr-warning' : ''}"><%----%>
                    <div class="td td-label">Name:</div><%----%>
                    <div class="td td-content"><%----%>
                        <span>${keyInfo.name}</span><%----%>
                        <c:if test="${showNameWarning}">
                            <br><span class="keyName">${product.value.KeyName}</span><%----%>
                            <br><span class="message">Achtung: Name in Schlüssel ungleich Produkt!</span><%----%>
                        </c:if>
                    </div><%----%>
                </div><%----%>

                <c:set var="showIdWarning" value="${not empty product and (product.value.BillingId.toString ne keyInfo.billingId)}" />
                <div class="tr ocee-key-monospace ocee-key-billing-id ${showIdWarning ? 'tr-warning' : ''}"><%----%>
                    <div class="td td-label">Installations-ID:</div><%----%>
                    <div class="td td-content"><%----%>
                        <span>${keyInfo.billingId}</span><%----%>
                        <c:if test="${showIdWarning}">
                            <br><span class="keyid">${product.value.BillingId}</span><%----%>
                            <br><span class="message">Achtung: Installations-ID in Schlüssel ungleich Produkt!</span><%----%>
                        </c:if>
                    </div><%----%>
                </div><%----%>

                <div class="tr ocee-key-distribution ${showKeyTypeWarning ? 'tr-warning' : ''}"><%----%>
                    <div class="td td-label">Variante:</div><%----%>
                    <div class="td td-content"><%----%>
                        <c:choose>
                            <c:when test="${keyInfo.productSupport eq 1}">
                                <fmt:message key="product.distribution.oceeV1ServerEnhancement" />
                            </c:when>
                            <c:when test="${keyInfo.productSupport eq 2}">
                                <fmt:message key="product.distribution.oceeV1Cluster" />
                            </c:when>
                            <c:when test="${keyInfo.productSupport eq 3}">
                                <fmt:message key="product.distribution.oceeV2Subscription" />
                            </c:when>
                            <c:otherwise>
                                <span>'${keyInfo.productSupport}' - Unbekannt!</span><%----%>
                            </c:otherwise>
                        </c:choose>
                    </div><%----%>
                </div><%----%>

                <div class="tr ocee-key-type ocee-key-type-fromkey"><%----%>
                    <div class="td td-label">Typ:</div><%----%>
                    <div class="td td-content"><%----%>
                        ${keyInfo.type}
                    </div><%----%>
                </div><%----%>

                <c:if test="${not empty value and value.Date.isSet}">
                    <div class="tr ocee-key-date"><%----%>
                        <div class="td td-label">Erzeugt am:</div><%----%>
                        <div class="td td-content"><%----%>
                            <c:set var="instanceDate" value="${value.Date.toInstanceDate}" />
                            <c:set var="ignore" value="${instanceDate.setWholeDay(true)}" />
                            ${instanceDate.formatShort}
                        </div><%----%>
                    </div><%----%>
                </c:if>

                <c:choose>
                    <c:when test="${keyInfo.type eq 'development'}">
                        <div class="tr ocee-key-development"><%----%>
                            <div class="td td-label">Entwicklungszeit:</div><%----%>
                            <div class="td td-content">${keyInfo.validTill} Stunden</div><%----%>
                        </div><%----%>
                    </c:when>
                    <c:otherwise>
                        <div class="tr ocee-key-expiration"><%----%>
                            <div class="td td-label">Ablaufdatum:</div><%----%>
                            <div class="td td-content"><%----%>
                                <c:set var="instanceDate" value="${cms.wrap[keyInfo.validTill].toInstanceDate}" />
                                <c:set var="ignore" value="${instanceDate.setWholeDay(true)}" />
                                ${instanceDate.formatShort}
                            </div><%----%>
                        </div><%----%>
                    </c:otherwise>
                </c:choose>

                <div class="tr ocee-key-clustersize"><%----%>
                    <div class="td td-label">Knoten im Cluster:</div><%----%>
                    <div class="td td-content"><%----%>
                        ${keyInfo.maxClusterSize}
                    </div><%----%>
                </div><%----%>

                <div class="tr ocee-key-licence"><%----%>
                    <div class="td td-label">Schlüssel:</div><%----%>
                    <div class="td td-content"><%----%>
                        ${oceekey}
                    </div><%----%>
                </div><%----%>

                <div class="tr ocee-key-monospace ocee-key-base-id"><%----%>
                    <div class="td td-label">Base-ID:</div><%----%>
                    <div class="td td-content"><%----%>
                        ${keyInfo.baseId}
                    </div><%----%>
                </div><%----%>

                <div class="tr ocee-key-monospace ocee-key-json-data"><%----%>
                    <div class="td td-label">JSON Daten:</div><%----%>
                    <div class="td td-content"><%----%>
                        ${keyInfo.toJSONString()}
                    </div><%----%>
                </div><%----%>

                <c:if test="${value.ActivationKey.isSet}">
                    <div class="tr ocee-key-activation"><%----%>
                        <div class="td td-label">Aktivierungscode:</div><%----%>
                        <div class="td td-content"><%----%>
                            ${value.ActivationKey}
                        </div><%----%>
                    </div><%----%>
                </c:if>

            </c:when>

            <c:otherwise>
                <mercury:alert-online>
                    <jsp:attribute name="head">Schlüssel kann nicht angezeigt werden!</jsp:attribute>
                    <jsp:attribute name="text">Die Schlüsseldaten sind eventuell fehlerhaft.</jsp:attribute>
                </mercury:alert-online>
            </c:otherwise>

        </c:choose>

        <c:if test="${not empty value and (value.Inactive.toBoolean or value.DisplaySales.toBoolean)}">
            <div class="tr intra-inactive"><%----%>
                <div>Dieser Schlüssel ist deaktiviert. Es wird kein Support mehr für diesen Schlüssel geleistet!</div><%----%>
            </div><%----%>
        </c:if>

    </intranet:ocee-key-vars>

</div><%----%>
<mercury:nl />

</cms:bundle>
