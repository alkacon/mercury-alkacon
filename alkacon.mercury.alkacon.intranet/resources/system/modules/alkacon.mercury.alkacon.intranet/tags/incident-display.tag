<%@ tag pageEncoding="UTF-8"
    display-name="contact-display"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays intranet contact information." %>


<%@ attribute name="incident" type="java.lang.Object" required="true"
    description="The incident to display." %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:set var="value" value="${incident.value}" />

<c:if test="${(not empty value.Date) or (not empty value.Description) or (not empty value.HandledBy)}">

    <mercury:nl />
    <div class="intra-table loose intra-block intra-block-incident product-box ${value.Closed.toBoolean ? 'status-active' : 'status-v1invalid'}"><%----%>
        <c:if test="${not empty value.Date}">
           <c:set var="instDate" value="${value.Date.toInstanceDate}" />
            <c:set var="ignore"  value="${instDate.setWholeDay(true)}" />
            <div class="tr incident-date"><%----%>
                <div class="td td-label">Datum:</div><%----%>
                <div class="td td-content">${instDate.formatShort}</div><%----%>
            </div><%----%>
        </c:if>
        <c:if test="${not empty value.Description}">
            <div class="tr incident-description"><%----%>
                <div class="td td-label">Description:</div><%----%>
                <div class="td td-content">${value.Description}</div><%----%>
            </div><%----%>
        </c:if>
        <c:if test="${not empty value.HandledBy}">
            <div class="tr incident-handled-by"><%----%>
                <div class="td td-label">Bearbeitet von:</div><%----%>
                <div class="td td-content">${value.HandledBy}</div><%----%>
            </div><%----%>
        </c:if>
        <c:if test="${value.Closed.toBoolean}">
            <div class="tr incident-closed"><%----%>
                <div class="td td-label"></div><%----%>
                <div class="td td-content">Dieser Incident ist abgeschlossen!</div><%----%>
            </div><%----%>
        </c:if>
    </div><%----%>
    <mercury:nl />

</c:if>