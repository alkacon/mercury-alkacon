<%@ tag pageEncoding="UTF-8"
    display-name="sort-products"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    import="
        java.util.*
    "
    description="Sorts a list of products." %>


<%@ attribute name="products" type="java.util.List" required="true"
    description="The products to sort." %>


<%@ variable name-given="sortedProducts"            declare="true" %>
<%@ variable name-given="sortedProductsValid"       declare="true" %>
<%@ variable name-given="sortedProductsInvalid"     declare="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<jsp:useBean id="sortedProducts"                class="java.util.ArrayList" />
<jsp:useBean id="sortedProductsValid"           class="java.util.ArrayList" />
<jsp:useBean id="sortedProductsInvalid"         class="java.util.ArrayList" />
<jsp:useBean id="lookupTime"                    class="java.util.ArrayList" />
<jsp:useBean id="lookupIsInactive"              class="java.util.ArrayList" />

<c:if test="${not empty products}">

    <c:forEach var="productItem" items="${products}">
        <c:set var="product" value="${cms:isWrapper(productItem) ? productItem : productItem.toXml}" />
        <intranet:product-vars product="${product}">
            <c:set var="expirationTime" value="${expirationDate.start.time}" />
            <c:set var="added" value="${false}" />
            <c:forEach var="checkItem" items="${lookupTime.clone()}" varStatus="status">
                <c:if test="${(not added) and (checkItem < expirationTime)}">
                    <c:set var="ignore" value="${sortedProducts.add(status.index, productItem)}" />
                    <c:set var="ignore" value="${lookupTime.add(status.index, expirationTime)}" />
                    <c:set var="ignore" value="${lookupIsInactive.add(status.index, isInactive)}" />
                    <c:set var="added" value="${true}" />
                </c:if>
            </c:forEach>
            <c:if test="${not added}">
                <c:set var="ignore" value="${sortedProducts.add(productItem)}" />
                <c:set var="ignore" value="${lookupTime.add(expirationTime)}" />
                <c:set var="ignore" value="${lookupIsInactive.add(isInactive)}" />
            </c:if>
        </intranet:product-vars>
    </c:forEach>

    <c:forEach var="productItem" items="${sortedProducts}" varStatus="status">
        <c:choose>
            <c:when test="${lookupIsInactive.get(status.index)}">
                <c:set var="ignore" value="${sortedProductsInvalid.add(productItem)}" />
            </c:when>
            <c:otherwise>
                <c:set var="ignore" value="${sortedProductsValid.add(productItem)}" />
            </c:otherwise>
        </c:choose>
    </c:forEach>

</c:if>

<jsp:doBody />