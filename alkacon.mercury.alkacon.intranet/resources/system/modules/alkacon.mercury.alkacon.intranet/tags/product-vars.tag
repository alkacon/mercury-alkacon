<%@ tag pageEncoding="UTF-8"
    display-name="product-vars"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    description="Provides convenient access variables for a product." %>


<%@ attribute name="product" type="java.lang.Object" required="true"
    description="The product to provide the variables for." %>

<%@ attribute name="locale" type="java.lang.String" required="false"
    description="The locale to use in resource bundles. Default is 'de'. The only other valid option is 'en'" %>


<%@ variable name-given="productType"       declare="true" %>
<%@ variable name-given="isOcee"            declare="true" %>
<%@ variable name-given="isPremiumSpport"   declare="true" %>
<%@ variable name-given="isIncidentSpport"  declare="true" %>
<%@ variable name-given="isEval"            declare="true" %>
<%@ variable name-given="isSubscription"    declare="true" %>
<%@ variable name-given="isAddNode"         declare="true" %>
<%@ variable name-given="isUp12"            declare="true" %>
<%@ variable name-given="isProductV1"       declare="true" %>
<%@ variable name-given="isProductV2"       declare="true" %>
<%@ variable name-given="isKeyTypeV1"       declare="true" %>
<%@ variable name-given="isKeyTypeV2"       declare="true" %>
<%@ variable name-given="isV1Combination"   declare="true" %>
<%@ variable name-given="isV1Enhancement"   declare="true" %>
<%@ variable name-given="isV1Cluster"       declare="true" %>

<%@ variable name-given="startDate"         declare="true" %>
<%@ variable name-given="activationDate"    declare="true" %>
<%@ variable name-given="activationDateStr" declare="true" %>
<%@ variable name-given="expirationDate"    declare="true" %>
<%@ variable name-given="expirationDateStr" declare="true" %>
<%@ variable name-given="expirationGuess"   declare="true" %>
<%@ variable name-given="activeDateRange"   declare="true" %>

<%@ variable name-given="oceeNodesBase"     declare="true" %>
<%@ variable name-given="oceeNodesAdd"      declare="true" %>
<%@ variable name-given="oceeNodesTotal"    declare="true" %>

<%@ variable name-given="isExpired"         declare="true" %>
<%@ variable name-given="isInactive"        declare="true" %>
<%@ variable name-given="isPaid"            declare="true" %>

<%@ variable name-given="productStatus"     declare="true" %>
<%@ variable name-given="isStatusInactive"  declare="true" %>
<%@ variable name-given="isStatusv1Invalid" declare="true" %>
<%@ variable name-given="isStatusUnpaid"    declare="true" %>
<%@ variable name-given="isStatusExpired"   declare="true" %>
<%@ variable name-given="isStatusActive"    declare="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:set var="locale"                     value="${(locale eq 'en') ? locale : 'de'}" />

<c:choose>
    <c:when test="${product['class'].simpleName eq 'CmsJspContentAccessBean'}">
        <c:set var="typeName"           value="${product.resource.typeName}" />
        <c:set var="hasOwnProductFile"  value="${true}" />
        <c:set var="value"              value="${product.value}" />
    </c:when>
    <c:when test="${product['class'].simpleName eq 'CmsJspResourceWrapper'}">
        <c:set var="typeName"           value="${product.typeName}" />
        <c:set var="hasOwnProductFile"  value="${true}" />
        <c:set var="value"              value="${product.toXml.value}" />
    </c:when>
    <c:otherwise>
        <c:set var="typeName"           value="wrapper" />
        <c:set var="hasOwnProductFile"  value="${false}" />
        <c:set var="value"              value="${product.value}" />
    </c:otherwise>
</c:choose>

<%-- What kind of product is this? --%>
<c:set var="productType"                value="${value.ProductType.toString}" />

<c:set var="isOcee"                     value="${fn:containsIgnoreCase(productType, 'ocee') and (typeName eq 'supportOcee')}" />
<c:set var="isSubscription"             value="${fn:containsIgnoreCase(productType, 'sub')}" />
<c:set var="isEval"                     value="${isOcee and fn:containsIgnoreCase(productType, 'eval')}" />
<c:set var="isAddNode"                  value="${isOcee and fn:containsIgnoreCase(productType, 'acn')}" />
<c:set var="isV1Enhancement"            value="${isOcee and fn:containsIgnoreCase(productType, '1sl')}" />
<c:set var="isV1Cluster"                value="${isOcee and fn:containsIgnoreCase(productType, '2cnl')}" />
<c:set var="isUp12"                     value="${isOcee and fn:containsIgnoreCase(productType, 'up12')}" />
<c:set var="isV1Combination"            value="${fn:containsIgnoreCase(productType, '1sl-03') or fn:containsIgnoreCase(productType, '2cnl-03') or fn:containsIgnoreCase(productType, '1sl-02') or fn:containsIgnoreCase(productType, '2cnl-02')}" />
<c:set var="isPremiumSpport"            value="${not isOcee and (fn:containsIgnoreCase(productType, 'premium') or (isV1Combination and  fn:containsIgnoreCase(productType, '-03')))}" />
<c:set var="isIncidentSpport"           value="${not isOcee and (fn:containsIgnoreCase(productType, 'incident') or (isV1Combination and  fn:containsIgnoreCase(productType, '-02')))}" />

<c:set var="isProductV2"                value="${isSubscription or (isIncidentSpport and not isV1Combination)}" />
<c:set var="isProductV1"                value="${not isProductV2}" />

<%-- Set expected default in case there is a problem reading the activation / expiration dates --%>
<c:set var="isExpired"                  value="${true}" />

<%-- Activation date --%>
<c:set var="actDate"                    value="${value.ActivationDate.toDate}" />
<jsp:useBean id="activationDate"        class="org.opencms.jsp.util.CmsJspInstanceDateBean" />
<c:set var="ignore"                     value="${activationDate.init(actDate, locale)}" />
<c:set var="ignore"                     value="${activationDate.setWholeDay(true)}" />
<c:set var="activationDateStr"          value="${activationDate.formatShort}" />

<%-- Expiration date --%>
<c:set var="expirationGuess"            value="${false}" />
<c:set var="expDate"                    value="${value.ExpirationDate.toDate}" />
<c:if test="${not value.ExpirationDate.isSet}">
    <%-- In case expiration date has not been set, use some "old school" logic to get a reasonable default --%>
    <c:set var="expirationGuess"        value="${true}" />
    <c:set var="expirationDateTime"     value="${actDate.time + (365 * 24 * 60 * 60 * 1000)}" />
    <c:set var="ignore"                 value="${expDate.setTime(expirationDateTime)}" />
</c:if>
<jsp:useBean id="expirationDate"        class="org.opencms.jsp.util.CmsJspInstanceDateBean" />
<c:set var="ignore"                     value="${expirationDate.init(expDate, locale)}" />
<c:set var="ignore"                     value="${expirationDate.setWholeDay(true)}" />
<c:set var="expirationDateStr"          value="${expirationDate.formatShort}" />

<jsp:useBean id="currentDate"           class="java.util.Date" />
<c:set var="isExpired"                  value="${currentDate.time > expirationDate.start.time}" />

<%-- Active date range--%>
<jsp:useBean id="activeDateRange"       class="org.opencms.jsp.util.CmsJspInstanceDateBean" />
<c:set var="ignore"                     value="${activeDateRange.init(activationDate.start, locale)}" />
<c:set var="ignore"                     value="${activeDateRange.setEnd(expirationDate.start)}" />
<c:set var="ignore"                     value="${activeDateRange.setWholeDay(true)}" />

<%-- The allowed Number of nodes in the OCEE installation --%>

<c:if test="${isOcee}">
    <c:set var="isKeyTypeV1"                    value="${fn:contains(value.KeyType.toString, 'ocee-v1')}" />
    <c:set var="isKeyTypeV2"                    value="${not isKeyTypeV1}" />
    <c:set var="clusterAmount"                  value="${value.ClusterAmount.toInteger}" />
    <c:choose>
        <c:when test="${isProductV1}">
            <c:set var="oceeNodesBase"          value="${2}" />
            <c:set var="oceeNodesAdd"           value="${(isUp12 and (clusterAmount > 1)) ? (clusterAmount - 2) : clusterAmount}" />
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${fn:containsIgnoreCase(productType, 'sub-10')}">
                    <c:set var="oceeNodesBase"  value="${10}" />
                </c:when>
                <c:when test="${fn:containsIgnoreCase(productType, 'sub-05')}">
                    <c:set var="oceeNodesBase"  value="${5}" />
                </c:when>
                <c:otherwise>
                    <c:set var="oceeNodesBase"  value="${2}" />
                </c:otherwise>
            </c:choose>
            <c:set var="oceeNodesAdd"           value="${clusterAmount}" />
        </c:otherwise>
    </c:choose>
    <c:set var="oceeNodesTotal"                 value="${oceeNodesBase + oceeNodesAdd}" />
</c:if>

<%-- Quick access to paid and inactive flags --%>
<c:set var="isPaid"                     value="${value.Paid.toBoolean}" />
<c:set var="isInactive"                 value="${value.Inactive.toBoolean}" />

<c:choose>
    <c:when test="${isInactive}">
        <c:set var="productStatus"      value="status-inactive" />
        <c:set var="isStatusInactive"   value="${true}" />
    </c:when>
    <c:when test="${not isPaid}">
        <c:set var="productStatus"      value="status-unpaid" />
        <c:set var="isStatusUnpaid"     value="${true}" />
    </c:when>
    <c:when test="${isExpired and (isProductV1 or isIncidentSpport)}">
        <c:set var="productStatus"      value="status-v1invalid" />
        <c:set var="isStatusv1Invalid"  value="${true}" />
    </c:when>
    <c:when test="${isExpired and isProductV2}">
        <c:set var="productStatus"      value="status-expired" />
        <c:set var="isStatusExpired"    value="${true}" />
    </c:when>
    <c:otherwise>
        <c:set var="productStatus"      value="status-active" />
        <c:set var="isStatusActive"     value="${true}" />
    </c:otherwise>
</c:choose>

<jsp:doBody />
