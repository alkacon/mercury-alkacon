<%@ tag pageEncoding="UTF-8"
    display-name="ocee-key-vars"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    import="
        java.util.*,
        org.opencms.ocee.base.*,
        org.opencms.ocee.keygenerator.*,
        org.opencms.jsp.util.*
    "
    description="Decodes an OCEE key." %>


<%@ attribute name="key" type="java.lang.String" required="true"
    description="The license key string." %>

<%@ variable name-given="keyInfo"           declare="true" %>
<%@ variable name-given="keyType"           declare="true" %>
<%@ variable name-given="keyException"      declare="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty key}">
<%
    String key = (String)(jspContext.getAttribute("key"));

    if (key.length() > 80) {
        // this is a V2 key
        try {
            CmsLicenseInfo info = new CmsLicenseInfo(LicenseKeyGenerator.decodeLicenseKey(key));
            jspContext.setAttribute("keyInfo", info);
            jspContext.setAttribute("keyType", "v2");
        } catch (Exception e) {
            jspContext.setAttribute("keyException", e);
        }
    } else {
        // this is a V1 key
        try {
            CmsLicenseKeyChecker checker = new CmsLicenseKeyChecker();
            Map result = checker.getKeyInfo(key);
            jspContext.setAttribute("keyInfo", result);
            jspContext.setAttribute("keyType", "v1");
        } catch (Exception e) {
            jspContext.setAttribute("keyException", e);
        }
    }
%>
</c:if>

<jsp:doBody />
