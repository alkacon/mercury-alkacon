<%@ tag pageEncoding="UTF-8"
    display-name="mail-buttons"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays mail buttons for products." %>

<%@ attribute name="product" type="java.lang.Object" required="true"
    description="The product to display the mail buttons for." %>

<%@ attribute name="mailtype" type="java.lang.String" required="false"
    description="The type of mail to display the buttons for.
    Can be 'product' or 'sales'. Default is 'product'." %>

<%@ attribute name="locale" type="java.lang.String" required="false"
    description="The locale to use in resource bundles.
    Default is 'de'. The only other valid option is 'en'" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:if test="${not product.value.Inactive.toBoolean}">

<c:set var="mailtype" value="${(mailtype eq 'sales') ? mailtype : 'product'}" />
<c:set var="locale" value="${(locale eq 'en') ? locale : 'de'}" />

<c:set var="buttons">

    <intranet:mail-vars locale="${locale}">

        <intranet:product-vars product="${product}">

            <jsp:useBean id="neededTypes" class="java.util.ArrayList" />
            <c:set var="ignore" value="${neededTypes.add(mailtype)}" />
            <c:if test="${isPremiumSpport}">
                <c:set var="ignore" value="${neededTypes.add('premium')}" />
            </c:if>
            <c:if test="${isIncidentSpport}">
                <c:set var="ignore" value="${neededTypes.add('incident')}" />
            </c:if>
            <c:if test="${isOcee and isProductV2}">
                <c:set var="ignore" value="${neededTypes.add('oceev2')}" />
            </c:if>
            <c:if test="${isOcee and isProductV1}">
                <c:set var="ignore" value="${neededTypes.add('oceev1')}" />
            </c:if>
            <c:if test="${isEval}">
                <c:set var="ignore" value="${neededTypes.add('eval')}" />
            </c:if>

            <c:forEach var="mail" items="${mailConfig.localeValueList[locale]['Mail']}" varStatus="status">

                <c:set var="mailTypes" value="${fn:toLowerCase(mail.value.ShowIn.toString)}" />
                <c:choose>
                    <c:when test="${empty mailTypes}">
                        <c:set var="showButton" value="${false}" />
                    </c:when>
                    <c:when test="${fn:contains(mailTypes, 'all') and fn:contains(mailTypes, mailtype)}">
                        <c:set var="showButton" value="${true}" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="showButton" value="${true}" />
                        <c:forEach var="needed" items="${neededTypes}">
                            <c:if test="${(not showButton) or (not fn:contains(mailTypes, needed))}">
                                <c:set var="showButton" value="${false}" />
                            </c:if>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>

                <c:if test="${showButton}">
                    <div class="intra-btn"><%----%>
                        <form method="get" action="${cms.functionDetail['Send Mail']}"><%----%>
                            <input type="submit" class="btn" value="${mail.value.Title} [${locale}]"><%----%>
                            <input type="hidden" name="product" value="${product.resource.sitePath}"><%----%>
                            <input type="hidden" name="selectedLocale" value="${locale}"><%----%>
                            <input type="hidden" name="selectedMail" value="${status.index}"><%----%>
                        </form><%----%>
                    </div><%----%>
                </c:if>

            </c:forEach>

        </intranet:product-vars>

    </intranet:mail-vars>

</c:set>

<c:if test="${not empty buttons}">
    <mercury:nl />
    <div class="intra-btn-block intra-btn-block-mail"><%----%>
    ${buttons}
    </div><%----%>
    <mercury:nl />
</c:if>

</c:if>