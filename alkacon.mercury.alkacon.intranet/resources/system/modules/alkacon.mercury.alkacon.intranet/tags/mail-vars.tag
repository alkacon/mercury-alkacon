<%@ tag pageEncoding="UTF-8"
    display-name="product-vars"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    import="
        org.opencms.file.*,
        org.opencms.util.*,
        org.opencms.jsp.util.*
    "
    description="Provides convenient access to the intranet email configuration." %>


<%@ attribute name="locale" type="java.lang.String" required="false"
    description="The locale to use in resource bundles.
    Default is 'de'. The only other valid option is 'en'" %>

<%@ attribute name="customer" type="java.lang.Object" required="false"
    description="The customer to use for marcro resolving." %>

<%@ attribute name="product" type="java.lang.Object" required="false"
    description="The product to use for marcro resolving." %>


<%@ variable name-given="mailConfig"        declare="true" %>
<%@ variable name-given="mailRecipients"    declare="true" %>
<%@ variable name-given="macros"            declare="true" variable-class="org.opencms.util.CmsMacroResolver" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:set var="locale" value="${(locale eq 'en') ? locale : 'de'}" />

<%-- Check the configuration and read the email file --%>
<c:set var="mailConfig" value="${cms.wrap['/system/modules/alkacon.mercury.alkacon.intranet/configuration/mails.xml'].toResource.toXml}" />
<c:set var="downloadConfig" value="${cms.wrap['/system/modules/alkacon.mercury.alkacon.intranet/configuration/ocee-downloads.xml'].toResource.toXml}" />

<%-- Provide the macro resolver for the email. --%>
<%
    CmsObject cms = ((CmsJspStandardContextBean)getJspContext().findAttribute("cms")).getVfs().getCmsObject();
    CmsMacroResolver resolver = CmsMacroResolver.newInstance();
    resolver.setCmsObject(cms);
    resolver.setKeepEmptyMacros(true);
    getJspContext().setAttribute("macros", resolver);
    getJspContext().setAttribute("nl", "\n");
%>

<%-- Fill the special macros for the email. This is no fun but who cares? --%>
<fmt:setLocale value="${locale}" />
<cms:bundle basename="alkacon.mercury.alkacon.intranet.messages">

    <c:if test="${not empty customer}">

        <c:if test="${customer.value.Name.isSet}">
            <c:set var="ignore" value="${macros.addMacro('customerName', customer.value.Name)}" />
        </c:if>

        <c:if test="${not empty customer.valueList['Contact']}">
            <c:set var="customerContacts" value="${customer.valueList['Contact']}" />
        </c:if>

    </c:if>

    <c:if test="${not empty product}">

        <intranet:product-vars product="${product}" locale="${locale}">

            <c:set var="ignore" value="${macros.addMacro('signatureProduct', mailConfig.localeValue[locale].SignatureProduct)}" />
            <c:set var="ignore" value="${macros.addMacro('signatureSales', mailConfig.localeValue[locale].SignatureSales)}" />

            <c:if test="${product.value.ProductType.isSet}">
                <c:set var="productType"><fmt:message key="product.name.${product.value.ProductType}" /></c:set>
                <c:set var="ignore"         value="${macros.addMacro('productName', productType)}" />
            </c:if>

            <c:set var="ignore"             value="${macros.addMacro('activationDate', activationDateStr)}" />
            <c:set var="ignore"             value="${macros.addMacro('expirationDate', expirationDateStr)}" />

            <c:if test="${product.value.AccountID.isSet}">
                <c:set var="ignore"         value="${macros.addMacro('accountId', product.value.AccountID)}" />
            </c:if>

            <c:if test="${not empty product.valueList['Contact']}">
                <c:set var="productContacts" value="${product.valueList['Contact']}" />
            </c:if>

            <c:if test="${isOcee}">

                <c:if test="${product.value.KeyName.isSet}">
                    <c:set var="ignore"     value="${macros.addMacro('keyName', product.value.KeyName.toString)}" />
                </c:if>

                <c:choose>
                    <c:when test="${isProductV2}">
                        <c:set var="distribution"><fmt:message key="product.distribution.mail.oceeV2Subscription" /></c:set>
                    </c:when>
                    <c:when test="${isV1Enhancement}">
                        <c:set var="distribution"><fmt:message key="product.distribution.mail.oceeV1ServerEnhancement" /></c:set>
                    </c:when>
                    <c:otherwise>
                        <%-- New products must be cluster, even if key is V1 --%>
                        <c:set var="distribution"><fmt:message key="product.distribution.mail.oceeV1Cluster" /></c:set>
                    </c:otherwise>
                </c:choose>
                <c:set var="ignore" value="${macros.addMacro('distribution', distribution)}" />

                <c:choose>
                    <c:when test="${isKeyTypeV2}">
                        <c:set var="ignore" value="${macros.addMacro('keyNotes', mailConfig.localeValue[locale].V2KeyNotes)}" />
                        <c:set var="ignore" value="${macros.addMacro('keyNotesProvEval', mailConfig.localeValue[locale].V2KeyNotesProvEval)}" />
                        <c:set var="oceeProductionInformation" value="${mailConfig.localeValue[locale].V2KeyProduction}" />
                        <c:set var="oceeDevelopmentInformation" value="${mailConfig.localeValue[locale].V2KeyDevelopment}" />
                        <c:set var="oceeEvalProvisonalInformation" value="${mailConfig.localeValue[locale].V2KeyProvEval}" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="ignore" value="${macros.addMacro('keyNotes', mailConfig.localeValue[locale].V1KeyNotes)}" />
                        <c:set var="ignore" value="${macros.addMacro('keyNotesProvEval', mailConfig.localeValue[locale].V1KeyNotesProvEval)}" />
                        <c:set var="oceeProductionInformation" value="${mailConfig.localeValue[locale].V1KeyProduction}" />
                        <c:set var="oceeDevelopmentInformation" value="${mailConfig.localeValue[locale].V1KeyDevelopment}" />
                        <c:set var="oceeEvalProvisonalInformation" value="${mailConfig.localeValue[locale].V1KeyProvEval}" />
                    </c:otherwise>
                </c:choose>

                <c:set var="keys" value="${product.valueList['Key']}" />
                <c:if test="${not empty keys}">
                    <intranet:sort-keys keys="${keys}">
                        <c:forEach var="oceeKey" items="${sortedKeysValid}">

                            <c:if test="${oceeKey.value.LicenceKey.isSet}">
                                <c:set var="formattedKey"><intranet:format-license-key key="${oceeKey.value.LicenceKey}" /></c:set>
                                <c:set var="ignore" value="${macros.addMacro('licenseKey', formattedKey)}" />
                            </c:if>
                            <c:if test="${oceeKey.value.ActivationKey.isSet}">
                                <c:set var="formattedKey"><intranet:format-license-key key="${oceeKey.value.ActivationKey}" /></c:set>
                                <c:set var="ignore" value="${macros.addMacro('activationCode', formattedKey)}" />
                            </c:if>
                            <c:if test="${oceeKey.value.DevelopmentTime.isSet}">
                                <c:set var="ignore" value="${macros.addMacro('developmentTime', (oceeKey.value.DevelopmentTime.toInteger * 4))}" />
                            </c:if>
                            <c:if test="${oceeKey.value.ExpirationDate.isSet}">
                                <c:set var="date"><fmt:formatDate value="${oceeKey.value.ExpirationDate.toDate}" type="date" dateStyle="SHORT" /></c:set>
                                <c:set var="ignore" value="${macros.addMacro('keyExpirationDate', date)}" />
                            </c:if>
                            <c:if test="${oceeKey.value.Type.isSet}">
                                <c:set var="keyType"><fmt:message key="product.type.${fn:toLowerCase(oceeKey.value.Type)}" /></c:set>
                                <c:set var="ignore" value="${macros.addMacro('keyType', keyType)}" />
                            </c:if>

                            <c:choose>
                                <c:when test="${('Production' eq oceeKey.value.Type.toString) and (oceeProductionInformation.isSet) and (isKeyTypeV1 or (isKeyTypeV2 and empty productionLicense))}">
                                    <c:set var="keyInfo" value="${macros.resolveMacros(oceeProductionInformation)}" />
                                    <c:set var="productionLicense">${keyInfo}<mercury:nl /><mercury:nl />${productionLicense}</c:set>
                                </c:when>
                                <c:when test="${empty developmentLicense and ('Development' eq oceeKey.value.Type.toString) and (oceeDevelopmentInformation.isSet)}">
                                    <c:set var="keyInfo" value="${macros.resolveMacros(oceeDevelopmentInformation)}" />
                                    <c:set var="developmentLicense">${keyInfo}</c:set>
                                </c:when>
                                <c:when test="${empty evalProvisonalLicense and (('Provisional' eq oceeKey.value.Type.toString) or ('Evaluation' eq oceeKey.value.Type.toString)) and oceeEvalProvisonalInformation.isSet}">
                                    <c:set var="keyInfo" value="${macros.resolveMacros(oceeEvalProvisonalInformation)}" />
                                    <c:set var="evalProvisonalLicense">${keyInfo}</c:set>
                                </c:when>
                            </c:choose>

                            <c:set var="ignore" value="${macros.addMacro('licenseKey', '%(licenseKey)')}" />
                            <c:set var="ignore" value="${macros.addMacro('activationCode', '%(activationCode)')}" />
                            <c:set var="ignore" value="${macros.addMacro('developmentTime', '%(developmentTime)')}" />
                            <c:set var="ignore" value="${macros.addMacro('keyExpirationDate', '%(keyExpirationDate)')}" />

                        </c:forEach>
                    </intranet:sort-keys>
                </c:if>

                <c:if test="${not empty productionLicense}">
                    <c:set var="ignore" value="${macros.addMacro('productionLicence', productionLicense)}" />
                </c:if>
                <c:if test="${not empty developmentLicense}">
                    <c:set var="ignore" value="${macros.addMacro('developmentLicence', developmentLicense)}" />
                </c:if>
                <c:if test="${not empty evalProvisonalLicense}">
                    <c:set var="ignore" value="${macros.addMacro('evalProvisonalLicense', evalProvisonalLicense)}" />
                </c:if>

            </c:if>

            <c:choose>
                <c:when test="${isKeyTypeV2}">
                    <c:set var="dlvalues" value="${downloadConfig.localeValueList[locale].oceeV2Subscription}" />
                </c:when>
                <c:when test="${isV1Enhancement}">
                    <c:set var="dlvalues" value="${downloadConfig.localeValueList[locale].oceeV1ServerEnhancement}" />
                </c:when>
                <c:otherwise>
                    <%-- New products must be cluster, even if key is V1 --%>
                    <c:set var="dlvalues" value="${downloadConfig.localeValueList[locale].oceeV1Cluster}" />
                </c:otherwise>
            </c:choose>

            <c:forEach var="dl" items="${dlvalues}">
                <c:if test="${empty latestVersion}">
                    <c:set var="latestVersion" value="${dl.value.Version}" />
                </c:if>
                <c:set var="downloadLinks">
                    ${downloadLinks}<mercury:nl /><mercury:nl />
                    ${dl.value.Text}<mercury:nl />
                    ${dl.value.Link.toLink}
                </c:set>
            </c:forEach>
            <c:if test="${not empty downloadLinks}">
                <c:set var="ignore">${macros.addMacro("downloadLinks", downloadLinks)}<mercury:nl /></c:set>
            </c:if>
            <c:if test="${not empty latestVersion}">
                <c:set var="ignore">${macros.addMacro("latestVersion", latestVersion)}<mercury:nl /></c:set>
            </c:if>

        </intranet:product-vars>
    </c:if>


    <c:if test="${(not empty productContacts) or (not empty customerContacts)}">

        <c:choose>
            <%-- Primitive way of removing duplicate contact entries --%>
            <c:when test="${not empty productContacts}">
                <c:set var="contactMails" value="" />
                <c:forEach var="contact" items="${productContacts}">
                    <c:set var="contactMails" value="${contactMails}|${contact.value.Email}" />
                </c:forEach>
                <c:set var="contacts" value="${productContacts}" />
                <c:forEach var="contact" items="${customerContacts}">
                    <c:if test="${not fn:contains(contactMails, contact.value.Email.toString)}">
                         <c:set var="ignore" value="${contacts.add(contact)}" />
                         <c:set var="contactMails" value="${contactMails}|${contact.value.Email}" />
                    </c:if>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <c:set var="contacts" value="${customerContacts}" />
            </c:otherwise>
        </c:choose>

        <c:forEach var="contact" items="${contacts}" varStatus="status">
            <c:if test="${contact.value.Email.isSet}">
                <c:choose>
                    <c:when test="${status.first}">
                        <c:set var="mailRecipients" value="${contact.value.Email.toString}" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="mailRecipients" value="${mailRecipients},${contact.value.Email.toString}" />
                    </c:otherwise>
                </c:choose>
            </c:if>
        </c:forEach>
        <c:set var="ignore" value="${macros.addMacro('mailRecipients', mailRecipients)}" />

        <c:set var="firstContact" value="${contacts.get(0)}" />
        <c:if test="${firstContact.value.Name.isSet}">
            <c:set var="ignore" value="${macros.addMacro('contactName', firstContact.value.Name)}" />
        </c:if>
        <c:if test="${firstContact.value.Email.isSet}">
            <c:set var="ignore" value="${macros.addMacro('accountEmail', firstContact.value.Email)}" />
        </c:if>

        <c:set var="contactNames">${firstContact.value.Name} [${firstContact.value.Email}]</c:set>
        <c:if test="${contacts.size() > 1}">
            <c:set var="secondContact" value="${contacts.get(1)}" />
        </c:if>
        <c:choose>
            <c:when test="${not empty secondContact}">
                 <c:set var="contactNames">
                    ${contactNames}<mercury:nl />
                    ${secondContact.value.Name} [${secondContact.value.Email}]<%--
             --%></c:set>
            </c:when>
            <c:otherwise>
                 <c:set var="contactNames">
                    ${contactNames}<mercury:nl />
                    <fmt:message key="mail.msg.onlyone.contact" />
                 </c:set>
            </c:otherwise>
        </c:choose>
        <c:set var="ignore" value="${macros.addMacro('contacts', contactNames)}" />

    </c:if>

</cms:bundle>


<jsp:doBody />
