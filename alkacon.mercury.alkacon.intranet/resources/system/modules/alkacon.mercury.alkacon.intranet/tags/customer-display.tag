<%@ tag pageEncoding="UTF-8"
    display-name="customer-display"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays intranet customer information." %>


<%@ attribute name="customer" type="java.lang.Object" required="true"
    description="The customer to display." %>

<%@ attribute name="locale" type="java.lang.String" required="false"
    description="The locale to use in resource bundles. Default is 'de'. The only other valid option is 'en'" %>

<%@ attribute name="addLink" type="java.lang.Boolean" required="false"
    description="Add a link to the customer. Default is 'false'." %>

<%@ attribute name="showInactiveWarning" type="java.lang.Boolean" required="false"
    description="Show a warning in case the customer has been marked as inactive. Default is 'true'." %>

<%@ attribute name="showNotes" type="java.lang.Boolean" required="false"
    description="Show the notes for the customer. Default is 'true'." %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<c:choose>
    <%-- Customer in product may not have been set --%>
    <c:when test="${not empty customer}">

        <c:set var="locale"                     value="${(locale eq 'en') ? locale : 'de'}" />
        <c:set var="showInactiveWarning"        value="${empty showInactiveWarning ? true : showInactiveWarning}" />
        <c:set var="showNotes"                  value="${empty showNotes ? true : showNotes}" />

        <c:choose>
            <c:when test="${customer['class'].simpleName eq 'CmsJspContentAccessBean'}">
                <c:set var="value"              value="${customer.value}" />
                <c:set var="link"               value="${customer.fileLink}" />
            </c:when>
            <c:when test="${customer['class'].simpleName eq 'CmsJspResourceWrapper'}">
                <c:set var="value"              value="${customer.toXml.value}" />
                <c:set var="link"               value="${customer.resource.link}" />
            </c:when>
        </c:choose>

        <c:choose>
            <c:when test="${not addLink or empty link}">
                <c:set var="tagOpen">div</c:set>
                <c:set var="tagClose">div</c:set>
            </c:when>
            <c:otherwise>
                <c:set var="tagOpen">a href="${link}"</c:set>
                <c:set var="tagClose">a</c:set>
            </c:otherwise>
        </c:choose>

        <mercury:nl />
        <div class="intra-intro-customer"><%----%>

            <mercury:nl />
            <${tagOpen} class="customer-title"><%----%>

                <h1 class="intra-heading customer-name"><%----%>
                    ${value.Name}
                </h1><%----%>

                <c:if test="${not empty value.Country}">
                    <div class="customer-country"><%----%>
                        ${value.Country}
                    </div><%----%>
                </c:if>

            </${tagClose}>
            <mercury:nl />

            <c:if test="${showInactiveWarning and value.Inactive.toBoolean}">
                <mercury:alert-online css="intra-inactive">
                    <jsp:attribute name="head">Kunde ist deaktiviert!</jsp:attribute>
                    <jsp:attribute name="text">
                        <div>Die Daten werden nur noch für Archivzwecke aufbewahrt.</div>
                        <div>Es wird kein Support mehr für diesen Kunden geleistet!</div>
                    </jsp:attribute>
                </mercury:alert-online>
            </c:if>

            <c:if test="${showNotes and not empty value.Notes}">
                <div class="notes"><%----%>
                    ${value.Notes}
                </div><%----%>
            </c:if>
        </div><%----%>
        <mercury:nl />

    </c:when>
    <c:otherwise>

        <mercury:alert type="warning">
            <jsp:attribute name="head">Kundeninformation fehlt!</jsp:attribute>
            <jsp:attribute name="text">In diesem Produkt ist keine Verknüpung zu einem Kunden hinterlegt.</jsp:attribute>
        </mercury:alert>

    </c:otherwise>
</c:choose>
