<%@ tag pageEncoding="UTF-8"
    display-name="sort-keys"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    import="
        java.util.*
    "
    description="Sorts a list of OCEE keys." %>


<%@ attribute name="keys" type="java.util.List" required="true"
    description="The OCEE keys to sort." %>


<%@ variable name-given="sortedKeys"            declare="true" %>
<%@ variable name-given="sortedKeysValid"       declare="true" %>
<%@ variable name-given="sortedKeysInvalid"     declare="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>


<jsp:useBean id="sortedKeys"                class="java.util.ArrayList" />
<jsp:useBean id="sortedKeysValid"           class="java.util.ArrayList" />
<jsp:useBean id="sortedKeysInvalid"         class="java.util.ArrayList" />
<jsp:useBean id="lookupTime"                class="java.util.ArrayList" />
<jsp:useBean id="lookupIsInactive"          class="java.util.ArrayList" />

<c:if test="${not empty keys}">

    <c:forEach var="key" items="${keys}">
        <c:choose>
            <c:when test="${key.value.ExpirationDate.isSet}">
                <c:set var="expirationTime" value="${key.value.ExpirationDate.toDate.time}" />
            </c:when>
            <c:when test="${key.value.Type.isSet and (fn:toLowerCase(key.value.Type) eq 'development')}">
                <c:set var="expirationTime" value="${key.value.Date.toDate.time}" />
            </c:when>
            <c:otherwise>
                <c:set var="expirationTime" value="${key.value.Date.toDate.time + (365 * 24 * 60 * 60 * 1000)}" />
            </c:otherwise>
        </c:choose>
        <c:set var="isInactive" value="${key.value.Inactive}" />
        <c:set var="added" value="${false}" />
        <c:forEach var="checkItem" items="${lookupTime.clone()}" varStatus="status">
            <c:if test="${(not added) and (checkItem <= expirationTime)}">
                <%-- Note: If two keys have the exact same epiration date, the key the comes last in the content 'wins' --%>
                <c:set var="ignore" value="${sortedKeys.add(status.index, key)}" />
                <c:set var="ignore" value="${lookupTime.add(status.index, expirationTime)}" />
                <c:set var="ignore" value="${lookupIsInactive.add(status.index, isInactive)}" />
                <c:set var="added" value="${true}" />
            </c:if>
        </c:forEach>
        <c:if test="${not added}">
            <c:set var="ignore" value="${sortedKeys.add(key)}" />
            <c:set var="ignore" value="${lookupTime.add(expirationTime)}" />
            <c:set var="ignore" value="${lookupIsInactive.add(isInactive)}" />
        </c:if>
    </c:forEach>

    <c:forEach var="keyItem" items="${sortedKeys}" varStatus="status">
        <c:choose>
            <c:when test="${lookupIsInactive.get(status.index).toBoolean}">
                <c:set var="ignore" value="${sortedKeysInvalid.add(keyItem)}" />
            </c:when>
            <c:otherwise>
                <c:set var="ignore" value="${sortedKeysValid.add(keyItem)}" />
            </c:otherwise>
        </c:choose>
    </c:forEach>

</c:if>

<jsp:doBody />