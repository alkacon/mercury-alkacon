<%@ tag pageEncoding="UTF-8"
    display-name="product-link"
    body-content="empty"
    trimDirectiveWhitespaces="true"
    description="Displays an intranet product link." %>


<%@ attribute name="link" type="java.lang.Object" required="true"
    description="The customer product link." %>

<%@ attribute name="text" type="java.lang.String" required="false"
    description="Text to use for the link." %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>

<c:if test="${not empty link}">

    <c:set var="name" value="${fn:toLowerCase(link.toString)}" />
    <c:set var="link" value="${link.toLink}" />

    <c:choose>
        <c:when test="${fn:contains(name, 'ocee')}">
            <c:set var="name" value="OCEE Schlüssel" />
        </c:when>
        <c:when test="${fn:contains(name, 'premium')}">
            <c:set var="name" value="Premium Support" />
        </c:when>
        <c:otherwise>
            <c:set var="name" value="Incident Support" />
        </c:otherwise>
    </c:choose>

    <a href="${link}">${name}</a><%----%>

</c:if>