<%@ tag pageEncoding="UTF-8"
    display-name="check-user"
    body-content="scriptless"
    trimDirectiveWhitespaces="true"
    description="Checks if the current has a certain role or group." %>

<%-- Currently only checks if the user is the guest user --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mercury" tagdir="/WEB-INF/tags/mercury" %>
<%@ taglib prefix="intranet" tagdir="/WEB-INF/tags/intranet" %>

<c:if test="${cms.requestContext.currentUser.guestUser}">
    <c:set var="restrict" value="${true}" />
</c:if>

<c:choose>
    <c:when test="${restrict}">

        <mercury:nl/>
        <div class="element"><%----%>
            <mercury:alert-online>
                <jsp:attribute name="head">Kein Zugriff!</jsp:attribute>
            </mercury:alert-online>
        </div><%----%>
        <mercury:nl/>

    </c:when>
    <c:otherwise>

        <jsp:doBody />

    </c:otherwise>
</c:choose>
