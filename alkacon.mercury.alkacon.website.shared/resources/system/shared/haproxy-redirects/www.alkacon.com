^/de/company/contact/directions\.html$ /de/unternehmen/anfahrt/
^/de/company/contact/form\.html$ /de/unternehmen/kontakt/
^/de/company/contact/index\.html$ /de/unternehmen/kontakt/
^/de/company/contact/map\.html$ /de/unternehmen/anfahrt/
^/de/company/customers\.html$ /de/unternehmen/kunden/
^/de/company/index\.html$ /de/unternehmen/
^/de/company/jobs/datenschutz\.html$ /de/datenschutzerklaerung/
^/de/company/jobs/form\.html$ /de/unternehmen/stellenangebote/bewerbung/
^/de/company/jobs/index\.html$ /de/unternehmen/stellenangebote/
^/de/company/jobs/java_entwickler\.html$ /de/unternehmen/stellenangebote/job/Java-Entwickler-aus-Leidenschaft-Open-Source-Software-w-m/
^/de/company/jobs/ /de/unternehmen/stellenangebote/
^/de/company/ /de/unternehmen/

^/de/impressum\.html$ /de/impressum/

^/de/privacy\.html$ /de/datenschutzerklaerung/

^/de/products/consulting\.html$ /de/opencms-support/beratung/
^/de/products/index\.html$ /de/unternehmen/produkte/uebersicht/
^/de/products/ocee/cluster_package/ /de/ocee/uebersicht/
^/de/products/ocee/cluster_package/accelerator\.html$ /de/ocee/uebersicht/accelerator/
^/de/products/ocee/cluster_package/cluster_manager\.html$ /de/ocee/uebersicht/cluster-manager/
^/de/products/ocee/cluster_package/index\.html$ /de/ocee/uebersicht/
^/de/products/ocee/cluster_package/ldap_connector\.html$ /de/ocee/uebersicht/ldap-connector/
^/de/products/ocee/cluster_package/replicator\.html$ /de/ocee/uebersicht/db-replicator/
^/de/products/ocee/index\.html$ /de/ocee/uebersicht/
^/de/products/ocee/ocee_compatibility\.html$ /de/ocee/kompatibilitaet/
^/de/products/ocee/ocee_license\.html$ /de/ocee/lizenzbestimmungen/
^/de/products/ocee/ocee_scope\.html$ /de/ocee/lizenzbestimmungen/
^/de/products/ocee/ocee_subscription\.html$ /de/ocee/lizenzbestimmungen/
^/de/products/ocee/releases/170712-alkacon-ocee-5\.5\.2\.html$ /de/unternehmen/artikel/OCEE-Version-5.5.2-Release-Notes/
^/de/products/ocee/releases/171018-alkacon-ocee-5\.5\.3\.html$ /de/unternehmen/artikel/OCEE-Version-5.5.3-Release-Notes/
^/de/products/ocee/releases/180517-alkacon-ocee-5\.5\.4\.html$ /de/unternehmen/artikel/OCEE-Version-5.5.4-Release-Notes/
^/de/products/ocee/releases/190904-alkacon-ocee-11-0-1\.html$ /de/unternehmen/artikel/OCEE-Version-11.0.1-Release-Notes/
^/de/products/ocee/releases\.html$ /de/ocee/versionen/
^/de/products/ocee/ /de/ocee/uebersicht/

^/de/products/order\.html$ /de/unternehmen/produkte/bestellen/

^/de/products/support/index\.html$ /de/opencms-support/uebersicht/
^/de/products/support/individual_opencms_support\.html$ /de/opencms-support/beratung/
^/de/products/support/opencms_incident_support\.html$ /de/opencms-support/incident/
^/de/products/support/opencms_premium_support\.html$ /de/opencms-support/premium/
^/de/products/support/support_subscription\.html$ /de/opencms-support/bedingungen/
^/de/products/support/support_terms\.html$ /de/opencms-support/bedingungen/
^/de/products/support/ /de/opencms-support/uebersicht/

^/de/products/training/advanced_api\.html$ /de/opencms-schulungen/fortgeschritten/
^/de/products/training/basic_template\.html$ /de/opencms-schulungen/basic/
^/de/products/training/complete\.html$ /de/opencms-schulungen/komplett/
^/de/products/training/index\.html$ /de/opencms-schulungen/uebersicht/
^/de/products/training/requirements\.html$ /de/opencms-schulungen/bedingungen/
^/de/products/training/workshops\.html$ /de/opencms-schulungen/workshops/
^/de/products/training/ /de/opencms-schulungen/uebersicht/

^/de/products/ /de/unternehmen/produkte/uebersicht/

^/de/terms\.html$ /de/nutzungsbedingungen/

^/de/home/contact\.html$ /de/unternehmen/kontakt/
^/de/home/customers\.html$ /de/unternehmen/kunden/
^/de/home/jobs\.html$ /de/unternehmen/stellenangebote/
^/de/home/news\.html$ /de/unternehmen/news/
^/de/home/ocee\.html$ /de/ocee/uebersicht/
^/de/home/references\.html$ /de/unternehmen/kunden/
^/de/home/ /de/

^/de/oamp/index\.html$ /de/
^/de/oamp/ /de/

^/de/search\.html$ /de/suche/

^/en/company/contact/directions\.html$ /en/company/directions/
^/en/company/contact/form\.html$ /en/company/contact/
^/en/company/contact/map\.html$ /en/company/directions/
^/en/company/customers\.html$ /en/company/customers/
^/en/company/index\.html$ /en/company/about-alkacon/
^/en/company/ /en/company/about-alkacon/

^/en/impressum\.html$ /en/impressum/

^/en/privacy\.html$ /en/privacy-policy/

^/en/products/index\.html$ /en/company/products/overview/
^/en/products/consulting\.html$ /en/opencms-support/consulting/
^/en/products/ocee/ /en/ocee/overview/
^/en/products/ocee/cluster_package/ /en/ocee/overview/
^/en/products/ocee/cluster_package/accelerator\.html$ /en/ocee/overview/accelerator/
^/en/products/ocee/cluster_package/cluster_manager\.html$ /en/ocee/overview/cluster-manager/
^/en/products/ocee/cluster_package/index\.html$ /en/ocee/overview/
^/en/products/ocee/cluster_package/ldap_connector\.html$ /en/ocee/overview/ldap-connector/
^/en/products/ocee/cluster_package/replicator\.html$ /en/ocee/overview/db-replicator/
^/en/products/ocee/index\.html$ /en/ocee/overview/
^/en/products/ocee/ocee_compatibility\.html$ /en/ocee/compatibility/
^/en/products/ocee/ocee_license\.html$ /en/ocee/terms/
^/en/products/ocee/ocee_scope\.html$ /en/ocee/terms/
^/en/products/ocee/ocee_subscription\.html$ /en/ocee/terms/
^/en/products/ocee/releases/170712-alkacon-ocee-5\.5\.2\.html$ /en/company/article/OCEE-version-5.5.2-release-notes/
^/en/products/ocee/releases/171018-alkacon-ocee-5\.5\.3\.html$ /en/company/article/OCEE-version-5.5.3-release-notes/
^/en/products/ocee/releases/180517-alkacon-ocee-5\.5\.4\.html$ /en/company/article/OCEE-Version-5.5.4-Release-Notes/
^/en/products/ocee/releases/190801-alkacon-ocee-11-0-1\.html$ /en/company/article/OCEE-Version-11.0.1-Release-Notes/
^/en/products/ocee/releases\.html$ /en/ocee/versions/

^/en/products/order\.html$ /en/company/products/order/

^/en/products/support/index\.html$ /en/opencms-support/overview/
^/en/products/support/individual_opencms_support\.html$ /en/opencms-support/consulting/
^/en/products/support/opencms_incident_support\.html$ /en/opencms-support/incident/
^/en/products/support/opencms_premium_support\.html$ /en/opencms-support/premium/
^/en/products/support/support_subscription\.html$ /en/opencms-support/terms/
^/en/products/support/support_terms\.html$ /en/opencms-support/terms/
^/en/products/support/ /en/opencms-support/overview/

^/en/products/training/advanced_api\.html$ /en/opencms-training/advanced/
^/en/products/training/basic_template\.html$ /en/opencms-training/basic/
^/en/products/training/complete\.html$ /en/opencms-training/complete/
^/en/products/training/index\.html$ /en/opencms-training/overview/
^/en/products/training/requirements\.html$ /en/opencms-training/requirements/
^/en/products/training/workshops\.html$ /en/opencms-training/workshops/
^/en/products/ /en/company/products/overview/
^/en/products/training/ /en/opencms-training/overview/

^/en/terms\.html$ /en/terms-of-use/

^/en/home/contact\.html$ /en/company/contact/
^/en/home/customers\.html$ /en/company/customers/
^/en/home/jobs\.html$ /en/company/jobs/
^/en/home/news\.html$ /en/company/news/
^/en/home/ocee\.html$ /en/ocee/overview/
^/en/home/references\.html$ /en/company/customers/
^/en/home/ /en/

^/en/oamp/index\.html$ /en/
^/en/oamp/ /en/

^/en/search\.html$ /en/search/
